trigger CaseTrigger on Case (before insert, after insert,before update, after update, after delete, after undelete) {

     Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true'; 
    
     // Before insert record to database
     if(Trigger.isBefore && Trigger.isInsert) 
     {              
        if(RunTrigger || Test.isRunningTest()){
            //Check validation rule
            CaseValidationRule.checkValidate(Trigger.New);
            
            CaseTriggerHandler.handleBeforeInsert(Trigger.New);
        }
    }
    
     // After insert record to database
     if(Trigger.isAfter && Trigger.isInsert) 
     {              
        if(RunTrigger || Test.isRunningTest()){
            CaseTriggerHandler.handleAfterInsert(Trigger.New);
        }
    }
    
    //Before update record to database
    if(Trigger.isBefore && Trigger.isUpdate){
        if(RunTrigger || Test.isRunningTest()){
            //Check validation rule
            CaseValidationRule.checkValidate(Trigger.New);
            
            CaseTriggerHandler.handleBeforeUpdate(Trigger.oldMap, Trigger.NewMap);
        }
    }
    
    //After update record to database
    if(Trigger.isAfter && Trigger.isUpdate){
         if(RunTrigger || Test.isRunningTest()){ 
            system.debug('Test Call trigger after update case record');
            CaseTriggerHandler.handleAfterUpdate(Trigger.oldMap, Trigger.NewMap);

             //Check send SMS
            caseSMSButtonCtl callSendSMS = new caseSMSButtonCtl();
            callSendSMS.checkSendSMS(Trigger.oldMap, Trigger.NewMap);
             
            //CXMServiceProvider
            CXMServiceProvider cxmProvider = new CXMServiceProvider();
            cxmProvider.integrateToCXM(Trigger.oldMap,Trigger.NewMap);

            //Send closed case date update to ECM
            UpdateToECMController callUpdateECM = new UpdateToECMController();
            callUpdateECM.caseClosedFromTrigger(Trigger.oldMap,Trigger.NewMap);
         }
    } 
}