({
	onInit : function(component, event, helper) {

		var choiceSelect = component.get("v.choiceSelect");
		var surveyAwnser = component.get("v.surveyAwnser");
		var value;

		if(surveyAwnser != null)
		{
			var choiceSelect = component.get("v.choiceSelect");
			var surveyAwnser = component.get("v.surveyAwnser");

			for(var question in surveyAwnser)
			{
				if(question == choiceSelect.mainQuestionId){
					for(var choice in surveyAwnser[question])
					{
						if(choice == choiceSelect.choice.Id)
						{
							value = surveyAwnser[question][choice];
							component.set("v.answerObj",value);

							component.set("v.isValue", value[0]);	
				
						}
					}
				}	
			}		
		}
		else
		{
			value = "";
			component.set("v.isValue", value);
		}

	},

	
	itemsChange: function(component, event, helper) {
		var AnswerValue = component.get("v.AnswerValue");
		
		if(AnswerValue == undefined || AnswerValue == "")
		{
			component.set("v.isChecked", false);
		}
		else
		{
			component.set("v.isChecked", true);
		}

		helper.updateData(component); 
    },
})