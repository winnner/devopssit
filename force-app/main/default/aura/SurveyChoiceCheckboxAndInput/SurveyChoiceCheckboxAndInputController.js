({	
	updateCheckboxChoice : function(component, event, helper) {
		var isChecked = event.target.checked;
		component.set("v.isChecked", isChecked);

		if(isChecked == false )
		{
			component.set("v.choiceVerified", false);
		}
		helper.updateData(component); 
	}, 


	init: function (component) {

		var choiceSelect = component.get("v.choiceSelect");
		var surveyAwnser = component.get("v.surveyAwnser");
		if( surveyAwnser[choiceSelect.mainQuestionId] != undefined )
		{
			var choiceObj = surveyAwnser[choiceSelect.mainQuestionId][choiceSelect.choice.Id];
			component.set("v.answerObj",choiceObj);
			
			if( choiceObj == undefined || choiceObj.choiceChecked == false)
			{
				component.set("v.isChecked", false);
			}
			else
			{
				component.set("v.isChecked", true);
			}
		}
		else
		{
			component.set("v.isChecked", false);
		}

	},

	itemsChange: function(component, event, helper) {
		var AnswerValue = component.get("v.AnswerValue");

		if(AnswerValue == " " + undefined)
		{
			component.set("v.isChecked", false);
		}
		else if(AnswerValue == "")
		{
			component.set("v.isChecked", false);
		}
		else
		{
			component.set("v.isChecked", true);
		}
		helper.updateData(component); 
    },

})