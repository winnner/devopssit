@isTest
public without sharing class fnaAvatarPassportFlagshipCtrl_Test {

    static fnaAvatarPassportFlagshipCtrl testClass;

    @isTest
    private static void Test_fnaAvatarPassportFlagshipCtrl() {
        List<Avatar_Master__c> listAvatarMaster = new List<Avatar_Master__c>();
        Avatar_Master__c avatarMaster = new Avatar_Master__c();
        avatarMaster.Segment__c = 'Default';
        listAvatarMaster.add(avatarMaster);
        insert listAvatarMaster;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Avatar_Master__c = listAvatarMaster[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<Question__c> listQuestion = new List<Question__c>();
        Question__c question = new Question__c();
        question.Progress__c = 50;
        question.Name = 'Q13';
        listQuestion.add(question);
        insert listQuestion;

        List<Questionnaire_Result__c> listQuestionnaireResults = new List<Questionnaire_Result__c>();
        Questionnaire_Result__c questionnaireResults = new Questionnaire_Result__c();
        questionnaireResults.FNA_Activity_Name__c = listFnaAct[0].Id;
        questionnaireResults.Question_ID__c = listQuestion[0].Id;
        questionnaireResults.Choice_Answer__c = 'B';
        listQuestionnaireResults.add(questionnaireResults);
        insert listQuestionnaireResults;

        // PageReference pageRef = Page.fnaAvatarPassportFlagship; // Add your VF page Name here
        // Test.setCurrentPage(pageRef);

        String fnaId = listFnaAct[0].Id;
        System.currentPageReference().getParameters().put('Id',fnaId);
        // System.currentPageReference().getParameters().put('Home', 'fna-tmbbankpws.cs58.force.com');

        testClass = new fnaAvatarPassportFlagshipCtrl();
    }
}