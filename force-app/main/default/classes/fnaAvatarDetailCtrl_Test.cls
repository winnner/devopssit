@isTest
public without sharing class fnaAvatarDetailCtrl_Test {

    @isTest
    private static void Test_stampOffSet() {
        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        String fnaActivityId = UrlHelper.encryptParams(listFnaAct[0].Id);
        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": null,"creditCards": null,"loans": null,"bancassurances": null,"investments": null,"treasuries": [],"tradeFinances": []}}';
        String responseBodyOSC16 = '{"status": {"code": "0000","description": "Success"},"bancassurances": null,"total": "0"}';

        fnaAvatarDetailCtrl.stampOffSet(fnaActivityId, responseBodyOSC14, responseBodyOSC16);
    }

    @isTest
    private static void Test_callServiceOSC14() {
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'runtrigger';
        Aconfig.Value__c = 'true';
        insert Aconfig;   

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Customer__c = listAccount[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<FNA_Product_Offering__c> listProductOffering = new List<FNA_Product_Offering__c>();
        FNA_Product_Offering__c productOffering = new FNA_Product_Offering__c();
        productOffering.Flag_Offset_product_holding__c = false;
        productOffering.FNA_Activity__c = listFnaAct[0].Id;
        listProductOffering.add(productOffering);
        insert listProductOffering;

        String fnaActivityId = UrlHelper.encryptParams(listFnaAct[0].Id);

        fnaAvatarDetailCtrl.callServiceOSC14(fnaActivityId);
    }

    @isTest
    private static void Test_processResponseWsOSC14() {
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'runtrigger';
        Aconfig.Value__c = 'true';
        insert Aconfig;   

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Customer__c = listAccount[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        Datetime mockDateTime = Datetime.now();
        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": null,"creditCards": null,"loans": null,"bancassurances": null,"investments": null,"treasuries": [],"tradeFinances": []}}';
        List<String> labels = new List<String>();
        String fnaActivityId = listFnaAct[0].Id;
        String accountId = listAccount[0].Id;
        fnaOSCServiceUtility.StateInfo state = new fnaOSCServiceUtility.StateInfo(fnaActivityId,accountId,responseBodyOSC14,mockDateTime); 

        fnaAvatarDetailCtrl.processResponseWsOSC14(labels, state);
    }

    @isTest
    private static void Test_callServiceOSC16() {
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'runtrigger';
        Aconfig.Value__c = 'true';
        insert Aconfig;   

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Customer__c = listAccount[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<FNA_Product_Offering__c> listProductOffering = new List<FNA_Product_Offering__c>();
        FNA_Product_Offering__c productOffering = new FNA_Product_Offering__c();
        productOffering.Flag_Offset_product_holding__c = false;
        productOffering.FNA_Activity__c = listFnaAct[0].Id;
        listProductOffering.add(productOffering);
        insert listProductOffering;

        String fnaActivityId = UrlHelper.encryptParams(listFnaAct[0].Id);

        fnaAvatarDetailCtrl.callServiceOSC16(fnaActivityId);
    }
    
    @isTest
    private static void Test_processResponseWsOSC16() {
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'runtrigger';
        Aconfig.Value__c = 'true';
        insert Aconfig;   

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Customer__c = listAccount[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        Datetime mockDateTime = Datetime.now();
        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": null,"creditCards": null,"loans": null,"bancassurances": null,"investments": null,"treasuries": [],"tradeFinances": []}}';
        List<String> labels = new List<String>();
        String fnaActivityId = listFnaAct[0].Id;
        String accountId = listAccount[0].Id;

        fnaOSCServiceUtility.StateInfo state = new fnaOSCServiceUtility.StateInfo(fnaActivityId,accountId,responseBodyOSC14,mockDateTime);

        fnaAvatarDetailCtrl.processResponseWsOSC16(labels, state);
    }

    @isTest
    private static void Test_getAvatarMasterDetail() {
        List<Avatar_Master__c> listAvatarMaster = new List<Avatar_Master__c>();
        Avatar_Master__c avatarMaster = new Avatar_Master__c();
        avatarMaster.Segment__c = 'test';
        listAvatarMaster.add(avatarMaster);
        insert listAvatarMaster;

        List<Question__c> listQuestion = new List<Question__c>();
        Question__c question = new Question__c();
        question.Progress__c = 50;
        question.Name = 'Q13';
        listQuestion.add(question);
        insert listQuestion;

        List<Questionnaire_Result__c> listQuestionnaireResults = new List<Questionnaire_Result__c>();
        Questionnaire_Result__c questionnaireResults = new Questionnaire_Result__c();
        questionnaireResults.Question_ID__c = listQuestion[0].Id;
        listQuestionnaireResults.add(questionnaireResults);
        insert listQuestionnaireResults;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Avatar_Master__c = listAvatarMaster[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        // Test ContentVersion For increase % Test Class
        // List<ContentVersion> cvl = new List<ContentVersion>(); 
        // ContentVersion cvlist = new Contentversion(); 
        // cvlist.Title = 'Example_Avatar_bullet_Example'; 
        // cvlist.PathOnClient = 'test'; 
        // cvlist.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body 1'); 
        // cvl.add(cvlist); 

        // ContentVersion cvlist2 = new Contentversion(); 
        // cvlist2.Title = 'Example2_Avatar_bullet_Example2'; 
        // cvlist2.PathOnClient = 'test2'; 
        // cvlist2.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body 2'); 
        // cvl.add(cvlist2); 
        // insert cvl;

        String idForm = UrlHelper.encryptParams(listFnaAct[0].Id);
        String urlDropOff = 'Mockup';

        fnaAvatarDetailCtrl.getAvatarMasterDetail(idForm, urlDropOff);
    }

    @isTest
    private static void Test_sendOTP() {
        List<OTP__c> listOtp = new List<OTP__c>();
        OTP__c otp = new OTP__c();
        listOtp.add(otp);
        insert listOtp;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        String phone = '0123456789';
        String fnaId = UrlHelper.encryptParams(listFnaAct[0].Id);
        String otpId = listOtp[0].Id;

        fnaAvatarDetailCtrl.sendOTP(phone, fnaId, otpId);
    }

    @isTest
    private static void Test_getImage() {
        fnaAvatarDetailCtrl.getImage();
    }

    @isTest
    private static void Test_getProductOffer() {
        List<AppConfig__c> listAconfig = new List<AppConfig__c>();
        AppConfig__c Aconfig = new AppConfig__c();
        Aconfig.Name = 'runtrigger';
        Aconfig.Value__c = 'true';
        listAconfig.add(Aconfig);

        AppConfig__c Aconfig2 = new AppConfig__c();
        Aconfig2.Name = 'DefaultOwner';
        Aconfig2.Value__c = '99999';
        listAconfig.add(Aconfig2);

        AppConfig__c Aconfig3 = new AppConfig__c();
        Aconfig3.Name = 'FilterRetailSegment';
        Aconfig3.Value__c = '5|6|7|8|9';
        listAconfig.add(Aconfig3);

        insert listAconfig;   

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<Avatar_Master__c> listAvatarMaster = new List<Avatar_Master__c>();
        Avatar_Master__c avatarMaster = new Avatar_Master__c();
        avatarMaster.Segment__c = 'Default';
        listAvatarMaster.add(avatarMaster);
        insert listAvatarMaster;

        List<Question__c> listQuestion = new List<Question__c>();
        Question__c question = new Question__c();
        question.Progress__c = 50;
        question.Name = 'Q9';
        listQuestion.add(question);
        insert listQuestion;

        List<RTL_product_master__c> listRTLProductMaster = new List<RTL_product_master__c>();
        RTL_product_master__c RTLProductMaster = new RTL_product_master__c();
        RTLProductMaster.FNA_Active__c = true;
        RTLProductMaster.FNA_Product_Name__c = 'Example';
        listRTLProductMaster.add(RTLProductMaster);
        insert listRTLProductMaster;

        List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
        FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
        productMapping.Segment__c = 'Default';
        productMapping.Product__c = listRTLProductMaster[0].Id;
        productMapping.Highlight_Product_Condition__c = 'Q9-A,Q10-C|Q9-B,Q10-C|Q9-C,Q10-C';
        productMapping.Normal_Product_Condition__c = 'Q9-A/B/C';
        listProductMapping.add(productMapping);

        FNA_Product_Mapping__c productMapping2 = new FNA_Product_Mapping__c();
        productMapping2.Segment__c = 'Default';
        productMapping2.Product__c = listRTLProductMaster[0].Id;
        productMapping2.Highlight_Product_Condition__c = 'Q9-A/B/C';
        productMapping2.Normal_Product_Condition__c = 'Q9-A,Q10-C|Q9-B,Q10-C|Q9-C,Q10-C';
        listProductMapping.add(productMapping2);

        insert listProductMapping;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Avatar_Master__c = listAvatarMaster[0].Id;
        fnaActivity.Customer__c = listAccount[0].Id;
        // fnaActivity.Questionnaire_Results__c = listQuestionnaireResults[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<Questionnaire_Result__c> listQuestionnaireResults = new List<Questionnaire_Result__c>();
        Questionnaire_Result__c questionnaireResults = new Questionnaire_Result__c();
        questionnaireResults.FNA_Activity_Name__c = listFnaAct[0].Id;
        questionnaireResults.Question_ID__c = listQuestion[0].Id;
        questionnaireResults.Choice_Answer__c = 'B';
        listQuestionnaireResults.add(questionnaireResults);
        insert listQuestionnaireResults;

        String fnaActivityId = UrlHelper.encryptParams(listFnaAct[0].Id);

        fnaAvatarDetailCtrl.getProductOffer(fnaActivityId);
    }

}