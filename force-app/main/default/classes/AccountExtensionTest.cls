@isTest 
global class AccountExtensionTest {
   /*   
    static testmethod void WebservicemockupTest(){
      
		TestUtils.createAppConfig();
		TestUtils.createUsers(1,'MockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'123TEST','Individual', true);
        TestUtils.createIdType();
		TestUtils.createStatuscode();        
        ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.accountList.get(0));
        AccountExtension AccEx = new AccountExtension(sc);
       	AccEx.isIDValid = true;
        AccEx.isInformation = true;
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchMock());
        Test.startTest();
        	AccEx.acctId = TestUtils.accountList.get(0).id;
        	AccEx.ownerId = TestUtils.userList.get(0).id;
        	List<Account> acctlist = AccEx.getAccounts();
        	AccEx.search();
        	TestUtils.accountList.get(0).Last_Name__c = null;
        	AccEx.search();
        	TestUtils.accountList.get(0).ID_Type_Temp__c = null;
        	AccEx.search();
        	System.assertEquals('Found duplication but can create',ErrorHandler.statusMessage);
        	AccEx.next();           
        	AccEx.viewOwner();
        	AccEx.viewProspect();
			AccEx.resetNextButton();
        	ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(TestUtils.accountList);
       		AccountExtension AccEx2 = new AccountExtension(ssc);
        Test.stopTest();
        
    }
    

    
    static testmethod void DatasNullTest(){
        
		TestUtils.createAppConfig();
		TestUtils.createUsers(1,'DataMockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'DataMockTEST','Individual', true);
        TestUtils.createIdType();
		TestUtils.createStatuscode();        
        ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.accountList.get(0));
        AccountExtension AccEx = new AccountExtension(sc);
        AccEx.isIDValid = true;
        AccEx.isInformation = true;
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchDatasNullMock());
        Test.startTest();
        	AccEx.acctId = TestUtils.accountList.get(0).id;
        	AccEx.ownerId = TestUtils.userList.get(0).id;
        	List<Account> acctlist = AccEx.getAccounts();
        	AccEx.search();
        Test.stopTest();
        
    }
    
     static testmethod void ResponseIsErrorTest(){
        
		TestUtils.createAppConfig();
		TestUtils.createUsers(1,'RespondMockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'RespondTEST','Individual', true);
        TestUtils.createIdType();
		TestUtils.createStatuscode();        
        ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.accountList.get(0));
        AccountExtension AccEx = new AccountExtension(sc);
        AccEx.isIDValid = true;
        AccEx.isInformation = true;
        Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        Test.setMock(WebServiceMock.class, new TMBSearchResponseIsErrorMock());
        Test.startTest();
        	AccEx.acctId = TestUtils.accountList.get(0).id;
        	AccEx.ownerId = TestUtils.userList.get(0).id;
        	List<Account> acctlist = AccEx.getAccounts();
        	AccEx.search();
        Test.stopTest();
        
    }
    

    
    static testmethod void CalloutExceptionTest(){
        Test.startTest();
        	TestUtils.createAppConfig();
		TestUtils.createUsers(1,'CalloutMockUp', 'AccountTest','AccountTest@tmb.com', true);
        TestUtils.createAccounts(1,'CalloutTEST','Individual', true);
        	TestUtils.createIdType();
			TestUtils.createStatuscode();        
        
        	ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.accountList.get(0));
        	AccountExtension AccEx = new AccountExtension(sc);
       		AccEx.isIDValid = true;
        	AccEx.isInformation = true;
        	Test.setMock(WebServiceMock.class, new TMBServiceProxyMockImpl());
        	Test.setMock(WebServiceMock.class, new TMBSearchMock());
        
        	AccEx.acctId = TestUtils.accountList.get(0).id;
        	AccEx.ownerId = TestUtils.userList.get(0).id;
        	List<Account> acctlist = AccEx.getAccounts();
        	AccEx.search();
        	System.assertEquals('Salesforce Error : Webservice Callout Error',ErrorHandler.statusMessage);
        Test.stopTest();
        
    }*/


}