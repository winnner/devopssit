@isTest
public without sharing class fnaHighlightProductCtrl_Test {

    @isTest
    private static void Test_idleTime() {
        fnaHighlightProductCtrl.idleTime();
    }

    @isTest
    private static void Test_createRef() {
        
        /*Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
       System.debug('What is the profile id ' + profile1);
       UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        
         User u = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'testtermsconditions1234423@kaplan.com',
            Alias = 'batman',
            Email='testtermsconditions1234423@kaplan.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago');
            insert u;
        
       	System.runas(u) {*/
        
            Avatar_Master__c avatar = new Avatar_Master__c();
            avatar.Avatar_Description__c = 'description';
            avatar.Avatar_EN__c = 'Avtar Name';
            avatar.Avatar_Segment__c = 'SME';
            avatar.Avatar_TH__c = 'นักบริหาร';
            avatar.Segment__c = 'SEM';
            avatar.Avatar_Short_Description__c = 'description';
            insert avatar;
                
            List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
            FNA_Activity__c fnaActivity = new FNA_Activity__c();
            fnaActivity.Branch__c = '555';
            fnaActivity.Avatar_Master__c = avatar.id;
            listFnaAct.add(fnaActivity);
            insert listFnaAct;
            
            List<RTL_product_master__c> listRTLProductMaster = new List<RTL_product_master__c>();
            RTL_product_master__c RTLProductMaster = new RTL_product_master__c();
            RTLProductMaster.FNA_Active__c = true;
            listRTLProductMaster.add(RTLProductMaster);
            insert listRTLProductMaster;
            
            List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
            FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
            productMapping.Product__c = listRTLProductMaster[0].Id;
            listProductMapping.add(productMapping);
            insert listProductMapping;
            
            FNA_Product_Offering__c offering = new FNA_Product_Offering__c();
            offering.FNA_Activity__c = listFnaAct[0].id;
            offering.FNA_Product_Name__c = listProductMapping[0].id;
            insert offering;
            
            Branch_and_Zone__c branch = new Branch_and_Zone__c();
            branch.Branch_Code__c = '555';
            branch.isActive__c = true;
            insert branch;
            
            List<String> appNameList = new List<String>();
            appNameList.add('RTL_Referral_Assignment_ISNULL');
            appNameList.add('RTL_Referral_Assignment_NOTNULL');
            appNameList.add('runReferralTrigger');
            List<AppConfig__c> appList = new List<AppConfig__c>();
            for(String str : appNameList){
                AppConfig__c app = new AppConfig__c();
                app.name = str;
                app.Value__c = 'true';
                appList.add(app);
            }
            insert appList;
            
            
            String fnaActId = UrlHelper.encryptParams(listFnaAct[0].Id);
            String sessionData = listRTLProductMaster[0].id;
            try{
                fnaHighlightProductCtrl.createRef(fnaActId, sessionData);
            }catch(Exception ex){
                
            }
            
        //}
        
    }

    @isTest
    private static void Test_getMapGroup() {
        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Avatar_Downloaded__c = false;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<RTL_product_master__c> listRTLProductMaster = new List<RTL_product_master__c>();
        RTL_product_master__c RTLProductMaster = new RTL_product_master__c();
        RTLProductMaster.FNA_Active__c = true;
        listRTLProductMaster.add(RTLProductMaster);
        insert listRTLProductMaster;

        List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
        FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
        productMapping.Product__c = listRTLProductMaster[0].Id;
        listProductMapping.add(productMapping);
        insert listProductMapping;

        List<FNA_Product_Offering__c> listProductOffering = new List<FNA_Product_Offering__c>();
        FNA_Product_Offering__c productOffering = new FNA_Product_Offering__c();
        productOffering.Flag_Offset_product_holding__c = false;
        productOffering.FNA_Product_Name__c = listProductMapping[0].Id;
        productOffering.FNA_Activity__c = listFnaAct[0].Id;
        productOffering.Flag_Highlight__c = true;
        listProductOffering.add(productOffering);

        FNA_Product_Offering__c productOffering2 = new FNA_Product_Offering__c();
        productOffering2.Flag_Offset_product_holding__c = false;
        productOffering2.FNA_Product_Name__c = listProductMapping[0].Id;
        productOffering2.FNA_Activity__c = listFnaAct[0].Id;
        productOffering2.Flag_Highlight__c = false;
        listProductOffering.add(productOffering2);
        insert listProductOffering;

        String fnaActivityId = UrlHelper.encryptParams(listFnaAct[0].Id);
        fnaHighlightProductCtrl.getMapGroup(fnaActivityId);
    }

    @isTest
    private static void Test_getThumbnailUrlAll() {

        List<RTL_product_master__c> listRTLProductMaster = new List<RTL_product_master__c>();
        RTL_product_master__c RTLProductMaster = new RTL_product_master__c();
        RTLProductMaster.FNA_Active__c = true;
        RTLProductMaster.FNA_Product_Name__c = 'Example_1';
        // RTLProductMaster.FNA_Product_Group_TH__c = 'บัญชีเพื่อใช้ - เพื่อออม'; //Field Not Writeable
        listRTLProductMaster.add(RTLProductMaster);
        insert listRTLProductMaster;

        List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
        FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
        productMapping.Product__c = listRTLProductMaster[0].Id;
        listProductMapping.add(productMapping);

        FNA_Product_Mapping__c productMapping2 = new FNA_Product_Mapping__c();
        productMapping.Product__c = listRTLProductMaster[0].Id;
        listProductMapping.add(productMapping2);
        insert listProductMapping;

        List<fnaHighlightProductCtrl.ProductFNA> listMockData = new List<fnaHighlightProductCtrl.ProductFNA>();
        fnaHighlightProductCtrl.ProductFNA mockData1 = new fnaHighlightProductCtrl.ProductFNA();
        mockData1.product = productMapping;
        listMockData.add(mockData1);
        fnaHighlightProductCtrl.ProductFNA mockData2 = new fnaHighlightProductCtrl.ProductFNA();
        mockData2.product = productMapping2;
        listMockData.add(mockData2);

        fnaHighlightProductCtrl.getThumbnailUrlAll(listMockData);
    }

    /*@isTest
    private static void Test_getThumbnailUrl() { //List index out of bound (can't mockup ContentVersion)
        List<RTL_product_master__c> listRTLProductMaster = new List<RTL_product_master__c>();
        RTL_product_master__c RTLProductMaster = new RTL_product_master__c();
        RTLProductMaster.FNA_Active__c = true;
        RTLProductMaster.FNA_Product_Name__c = '';
        // RTLProductMaster.FNA_Product_Group_TH__c = 'บัญชีเพื่อใช้ - เพื่อออม'; //Field Not Writeable
        listRTLProductMaster.add(RTLProductMaster);
        insert listRTLProductMaster;

        List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
        FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
        productMapping.Product__c = listRTLProductMaster[0].Id;
        listProductMapping.add(productMapping);
        insert listProductMapping;

        fnaHighlightProductCtrl.ProductFNA mockData1 = new fnaHighlightProductCtrl.ProductFNA();
        mockData1.product = listProductMapping[0];

        fnaHighlightProductCtrl.getThumbnailUrl(mockData1);
    }*/

    @isTest
    private static void Test_splitProductfunction() {
        List<FNA_Product_Mapping__c> listProductMapping = new List<FNA_Product_Mapping__c>();
        FNA_Product_Mapping__c productMapping = new FNA_Product_Mapping__c();
        productMapping.Segment__c = 'Default';
        // productMapping.Product__c = listRTLProductMaster[0].Id;
        productMapping.Highlight_Product_Condition__c = 'Q9-A,Q10-C|Q9-B,Q10-C|Q9-C,Q10-C';
        productMapping.Normal_Product_Condition__c = 'Q9-A/B/C';
        listProductMapping.add(productMapping);

        FNA_Product_Mapping__c productMapping2 = new FNA_Product_Mapping__c();
        productMapping2.Segment__c = 'Default';
        // productMapping2.Product__c = listRTLProductMaster[0].Id;
        productMapping2.Highlight_Product_Condition__c = 'Q9-A/B/C';
        productMapping2.Normal_Product_Condition__c = 'Q9-A,Q10-C|Q9-B,Q10-C|Q9-C,Q10-C';
        listProductMapping.add(productMapping2);

        insert listProductMapping;

        Map<String,String> mapAns = new Map<String,String>();
        mapAns.put('Q9','A');

        String fieldCondition = 'Highlight_Product_Condition__c';

        fnaHighlightProductCtrl.splitProductfunction(listProductMapping,mapAns,fieldCondition);
    }

    @isTest
    private static void Test_getAvatar() {
        List<Avatar_Master__c> listAvatarMaster = new List<Avatar_Master__c>();
        Avatar_Master__c avatarMaster = new Avatar_Master__c();
        avatarMaster.Segment__c = 'Default';
        listAvatarMaster.add(avatarMaster);
        insert listAvatarMaster;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Avatar_Master__c = listAvatarMaster[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        String idForm = UrlHelper.encryptParams(listFnaAct[0].Id);

        fnaHighlightProductCtrl.getAvatar(idForm);
    }

    @isTest
    private static void Test_getAvatarMasterDetail() {
        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<Question__c> listQuestion = new List<Question__c>();
        Question__c question = new Question__c();
        question.Progress__c = 50;
        question.Name = 'Q13';
        listQuestion.add(question);
        insert listQuestion;

        List<Questionnaire_Result__c> listQuestionnaireResults = new List<Questionnaire_Result__c>();
        Questionnaire_Result__c questionnaireResults = new Questionnaire_Result__c();
        questionnaireResults.FNA_Activity_Name__c = listFnaAct[0].Id;
        questionnaireResults.Question_ID__c = listQuestion[0].Id;
        questionnaireResults.Choice_Answer__c = 'B';
        listQuestionnaireResults.add(questionnaireResults);
        insert listQuestionnaireResults;

        String idForm = UrlHelper.encryptParams(listFnaAct[0].Id);
        String urlDropOff = 'Test';
        fnaHighlightProductCtrl.getAvatarMasterDetail(idForm, urlDropOff);
    }

    @isTest
    private static void Test_getAvatarImageUrl() {
        String segment = 'Default';
        String gender = 'ชาย';

        fnaHighlightProductCtrl.getAvatarImageUrl(segment, gender);
    }

    @isTest
    private static void Test_getAvatarName() {
        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<Question__c> listQuestion = new List<Question__c>();
        Question__c question = new Question__c();
        question.Progress__c = 50;
        question.Name = 'Q13';
        listQuestion.add(question);
        insert listQuestion;

        List<Questionnaire_Result__c> listQuestionnaireResults = new List<Questionnaire_Result__c>();
        Questionnaire_Result__c questionnaireResults = new Questionnaire_Result__c();
        questionnaireResults.FNA_Activity_Name__c = listFnaAct[0].Id;
        questionnaireResults.Question_ID__c = listQuestion[0].Id;
        questionnaireResults.Choice_Answer__c = 'B';
        listQuestionnaireResults.add(questionnaireResults);
        insert listQuestionnaireResults;

        String idForm = UrlHelper.encryptParams(listFnaAct[0].Id);
        fnaHighlightProductCtrl.getAvatarName(idForm);
    }

    @isTest
    private static void Test_sendOTP() {
        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<OTP__c> listOtp = new List<OTP__c>();
        OTP__c otp = new OTP__c();
        listOtp.add(otp);
        insert listOtp;

        String fnaId = UrlHelper.encryptParams(listFnaAct[0].Id);

        String phone = '0912345678';
        String otpId = listOtp[0].Id;
        fnaHighlightProductCtrl.sendOTP(phone, fnaId, otpId);
    }
}