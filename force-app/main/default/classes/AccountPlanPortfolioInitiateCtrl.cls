public without sharing class AccountPlanPortfolioInitiateCtrl { }
/*
    public AcctPlanPortfolio__c  portfolio {get;set;}
    public String YearStr {get;set;}
    private ApexPages.StandardController standardController {get;set;}
    public Set<String> GroupCompanySequenceSet {get;set;}
    public List<Target__c> TargetList {get;set;} 
    public Integer tempCustomerNum {get;set;}
    public Integer tempGroupNum {get;set;}
    List<Account> AccountList {get;set;}
    public Map<String,List<AcctPlanCompanyPort__c>> MapAccountwithgroup {get;set;}
    public Map<String,AcctPlanGroupPort__c> MapGroupPort {get;set;}
    public Map<Id,AcctPlanCompanyProfile__c> ExistingComProfileMap {get;set;}
    public List<id> GroupList {get;set;}
    public String selectedYear {get;set;}
    public Date currentDate {get;set;}
    public List<Date> AnnualDate {get;set;}
    public List<SelectOption> YearSelectOption {get;set;} 
    public Boolean hasNonGroup {get;set;}
    public Boolean isReadyToSave {get;set;}
    public String FiltersOption {get;set;}
    public Map<String,TMBAccountPlanServiceProxy.CUSTOMER_INFO> custinfoMap {get;set;}
    public Map<String,AccountPlanRefreshService.CustomerWalletInfo> walletinfoMap {get;set;}
    public List<SelectOption> getFiscalYear(){
        List<SelectOption> fiscalyearoption = new List<SelectOption>();
        List<Account_Plan_Fiscal_Year__c> yearlistitem = [SELECT ID,Name,AD_Year__c,BE_Year__c
                                                          FROM Account_Plan_Fiscal_Year__c 
                                                          WHERE ID!=null
                                                          ORDER BY Name];
        fiscalyearoption.add(new SelectOption('','None'));
        for(Account_Plan_Fiscal_Year__c year : yearlistitem){
            fiscalyearoption.add(new SelectOption(year.AD_Year__c,year.AD_Year__c));
        }         
        return fiscalyearoption;
    }
    public AccountPlanPortfolioInitiateCtrl(ApexPages.StandardController controller){
        currentDate = System.today();
        isReadyToSave = false;
        FiltersOption = 'Flag';
        AnnualDate = new List<Date>();
        GroupList = new List<id>();
        portfolio = new AcctPlanPortfolio__c();
        YearSelectOption = new List<SelectOption>();
        YearSelectOption.add(new SelectOption('','--None--'));
        YearSelectOption.add(new SelectOption('2014','2014'));
        YearSelectOption.add(new SelectOption('2015','2015'));
        YearSelectOption.add(new SelectOption('2016','2016'));
        YearSelectOption.add(new SelectOption('2017','2017'));
        YearSelectOption.add(new SelectOption('2018','2018'));
        YearSelectOption.add(new SelectOption('2019','2019'));
    }
    
    public void selectedYear(){
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO,
                                                   'After selecting year, please click Refresh button to view list of customers.'));
        
        portfolio.Year__c = YearStr;
        isReadyToSave = false;
        SetPortTableData(new List<Account>());
    }
    
    public pageReference Refreshfunction(){
        List<AcctPlanPortfolio__c> PortList = [SELECT ID,OwnerID,Year__C
                                               From AcctPlanPortfolio__c
                                               WHERE Year__c =: portfolio.Year__c
                                               AND SalesOwner__c =:Userinfo.getUserId()];
        
        if(PortList.size() > 0){
            
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                                                       'Duplicate Year :'+portfolio.Year__c+'. RM Portfolio already exists.'));
            
            return null;  
            
        }else{
            
            AccountList = [SELECT ID,Name,First_Name__c , Industry ,Last_Name__c,Group__c,Account_Plan_Flag__c,
                       Group__r.Name,
                       Group__r.GroupCompany__c ,
                       Group__r.GroupIndustry__c ,
                       Group__r.Parent_Company__c ,
                       Group__r.ParentIndustry__c,
                       Group__r.UltimateParent__c 
                      FROM Account
                      WHERE OwnerId =: Userinfo.getUserId()
                      AND Account_Plan_Flag__c ='Yes'
                      ORDER BY Group__r.Name];
            
            
            if(AccountList.size()>0){
                
                //Customer Infomation Service
                // TMBAccountPlanServiceProxy.CUSTOMER_INFO[] customerInfos = new List<TMBAccountPlanServiceProxy.CUSTOMER_INFO>(); 
                String tempids = '';
                for(Account acct : AccountList){
                    tempids += acct.id+',';
                }          
                
                String ids = tempids.substring(0,tempids.length()-1);
                
                
                //Existing wallet service
                //
                walletinfoMap  = new Map<String,AccountPlanRefreshService.CustomerWalletInfo>();
                List<AccountPlanRefreshService.CustomerWalletInfo> WalletInfoList = AccountPlanRefreshService.initialStep0(ids);
                if(WalletInfoList !=null){
                    if(WalletInfoList.size()>0){
                        for(AccountPlanRefreshService.CustomerWalletInfo walletInfo : WalletInfoList){
                            
                            walletinfoMap.put(walletInfo.Id,walletInfo);
                        }
                    }
                }
                
                //
                

                
                List<AcctPlanCompanyProfile__c> ExistingComprofileList = [SELECT ID,Year__C,Account__c,AcctPlanGroup__c, Portfolio__c
                                                                                          FROM AcctPlanCompanyProfile__c
                                                                                          WHERE Year__c =: portfolio.Year__c
                                                                                          AND Account__c IN: AccountList];  
                
                ExistingComProfileMap = new Map<Id,AcctPlanCompanyProfile__c>();
                
                for(AcctPlanCompanyProfile__c comprofile : ExistingComprofileList){
                    ExistingComProfileMap.put(comprofile.Account__c,comprofile);
                }
                
                
                
                
                SetPortTableData(AccountList);
                if(tempCustomerNum >0){
                    isReadyToSave= true;
                }
                //Set Target in portfolio
                portfolio.SalesOwner__c = Userinfo.getUserId();
                Date tempDate = currentDate;
                portfolio.AsOf__c = currentDate.year() +'-'+currentDate.month()+'-'+ currentDate.day() ;
                portfolio.RefreshAsOf__c = System.today();
                
                Date twelvemonths = tempDate.addMonths(-12);
                TargetList = AccountPlanUtilities.QueryTargetNIbyOwnerIDandYear(Userinfo.getUserId(),Integer.valueof(portfolio.Year__c));
                Decimal TargetNI = 0;
                for(Target__C target : TargetList){
                    
                    TargetNI +=target.NI_Target_Monthly__c;
                }
                portfolio.TargetNI__c =TargetNI;
            }else{
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Not found any account.'));
                
            }
            
            return null;  
            
        }
    }
    
    
    
    public void SetPortTableData(List<Account> AcctsList){
        
        hasnongroup = false;
        GroupCompanySequenceSet = new Set<String>();
        
        MapAccountwithgroup = new Map<String,List<AcctPlanCompanyPort__c>>();
        MapGroupPort = new Map<String,AcctPlanGroupPort__c>();
        List<AcctPlanGroupPort__c> groupportlist = new List<AcctPlanGroupPort__c>();
        for(Account acct : AcctsList){
            
            //Initiate AcctPort
            AcctPlanCompanyPort__c acctCom = new AcctPlanCompanyPort__c ();
            acctCom.Account__c = acct.id;
            acctCom.Account_Name__c = acct.Name;
            //acctCom.Name = ;
            acctCom.Account_Plan_Portfolio__c = portfolio.id;
            if(ExistingComProfileMap.containsKey(acct.id)){
                acctCom.Account_Plan_Company_Profile__c = ExistingComProfileMap.get(acct.id).id;
            }
            
            if(walletinfoMap.containsKey(acctCom.Account__c)){
                acctCom.Wallet__c = walletinfoMap.get(acctCom.Account__c).Wallet;
                acctCom.Performance__c = walletinfoMap.get(acctCom.Account__c).Annual;
                
                system.debug('::::walletinfoMap Set Value for ' + acctCom.Account__c +   ' | Wallet => ' + acctCom.Wallet__c + ' Performance => ' +acctCom.Performance__c );
                
                
            }
            
            //Group Assign
            if(acct.Group__c !=null){  
                //Has group
                String groupName = acct.Group__r.Name+'';
                GroupCompanySequenceSet.add(groupName);
                GroupList.add(acct.group__c);
                if(MapAccountwithgroup.containsKey(groupName)){
                    acctCom.AcctPlanGroupPort__c = MapGroupPort.get(groupName).id;
                    MapAccountwithgroup.get(groupName).add(acctCom);
                }else{
                    //New has group
                    List<AcctPlanCompanyPort__c> acctList = new List<AcctPlanCompanyPort__c>();
                    acctlist.add(acctCom);
                    MapAccountwithgroup.put(groupName,acctlist);
                    
                    AcctPlanGroupPort__c  groupport = new AcctPlanGroupPort__c ();
                    groupport.Account_Plan_Portfolio__c = portfolio.id;
                    groupport.Group_Name__c =acct.group__r.Name;
                    groupport.Name =acct.group__r.Name;
                    groupport.Group__c = acct.Group__c;
                    if(ExistingComProfileMap.containsKey(acct.id)){   
                        if(ExistingComProfileMap.get(acct.id).AcctPlanGroup__c !=null){
                            
                            groupport.Account_Plan_Group_Profile__c =ExistingComProfileMap.get(acct.id).AcctPlanGroup__c;
                        }
                    }
                    acctCom.AcctPlanGroupPort__c = groupport.id;
                    MapGroupPort.put(groupname, groupport);
                }
                
                
                
                
                //Initiate GroupPort
                
            }else{
                //Non group
                HasNongroup = true;
                if(MapAccountwithgroup.containsKey('None')){
                    MapAccountwithgroup.get('None').add(acctCom);
                    acctCom.AcctPlanGroupPort__c = MapGroupPort.get('None').id;
                }else{
                    //New non group
                    List<AcctPlanCompanyPort__c> acctList = new List<AcctPlanCompanyPort__c>();
                    acctlist.add(acctCom);
                    MapAccountwithgroup.put('None',acctlist); 
                    AcctPlanGroupPort__c  groupport = new AcctPlanGroupPort__c ();
                    groupport.Group_Name__c ='None';
                    groupport.Account_Plan_Portfolio__c = portfolio.id;
                    acctCom.AcctPlanGroupPort__c = groupport.id;
                    MapGroupPort.put('None', groupport);
                }
            }
            
        }
        
        tempGroupNum = GroupCompanySequenceSet.size();
        if(hasnongroup){
            GroupCompanySequenceSet.add('None');
            
        }
        
        tempCustomerNum =AcctsList.size();
        
        
    }
    
    public void AccountPlanFilters(){
        
        List<Account> FilterList = new List<Account>();
        if(FiltersOption=='Flag'){
            for(Account acct : AccountList){
                if(acct.Account_Plan_Flag__c =='Yes'){
                    
                    FilterList.add(acct);
                }
            }
        }else if(FiltersOption=='Group'){
            for(Account acct : AccountList){
                if(acct.Group__c !=null){
                    
                    FilterList.add(acct);
                }
            }
        }
        
        SetPortTabledata(FilterList);
        
    }
    
    
    
    public pageReference save(){
        List<AcctPlanCompanyPort__c> SelectedCustomerList = new  List<AcctPlanCompanyPort__c>();
        Set<AcctPlanGroupPort__c> SelectedGroupSet = new Set<AcctPlanGroupPort__c>();
        Map<String,List<AcctPlanCompanyPort__c>> GroupNamewithComMap = new Map<String,List<AcctPlanCompanyPort__c>>();
        double tempSumTargetRM = 0;
        if(portfolio.Year__c ==null){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Portforlio Year is null.'));
            
            return null;  
        }
        //case duplicate portfolio
        List<AcctPlanPortfolio__c> PortList = [SELECT ID,OwnerID,Year__C
                                               From AcctPlanPortfolio__c
                                               WHERE Year__c =: portfolio.Year__c
                                               AND SalesOwner__c =:Userinfo.getUserId()];
        if(PortList.size()>0){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Year :'+portfolio.Year__c+'. RM Portfolio already exists.'));
            return null; 
        }
        for(String groupname : GroupCompanySequenceSet){
            
            for(AcctPlanCompanyPort__c companyport : MapAccountwithgroup.get(groupname)) {
                
                if(companyport.Target_NI_By_RM__c !=null ){tempSumTargetRM +=companyport.Target_NI_By_RM__c;}
                
                if(groupname != 'None'){
                    SelectedGroupSet.add(MapGroupPort.get(groupname));
                }
                
                SelectedCustomerList.add(companyport);
                
                
                
            }
            
        }
        User currentuser = [SELECT ID,Employee_ID__c FROM User WHERE ID =: Userinfo.getUserID() LIMIT 1];
               
        if(SelectedCustomerList.size()==0){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You have to input Target NI by RM at least one record.'));
            
            return null;    
        }else if(currentuser.Employee_ID__c ==null){
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'User does not have Employee ID.'));
            
            return null;
        }else{
            try{
                portfolio.Status__c = 'In progress';
                portfolio.Name = currentuser.Employee_ID__c+' '+Userinfo.getFirstName()+' Portfolio: '+portfolio.Year__c;
                
                portfolio.SumOfTargetNIByRM__c = tempSumTargetRM;
                portfolio.RefreshAsOf__c = System.Now();
                
                insert portfolio;
                
                List <AcctPlanGroupPort__c> GroupInsertList = new List<AcctPlanGroupPort__c>();
                for(AcctPlanGroupPort__c groupport :  SelectedGroupSet){
                    groupport.Account_Plan_Portfolio__c = portfolio.id;
                    GroupInsertList.add(groupport);
                }
                
                insert GroupInsertList;
                
                Map<String,AcctPlanGroupPort__c> groupportMap = new Map<String,AcctPlanGroupPort__c>();
                for(AcctPlanGroupPort__c groupport :  SelectedGroupSet){
                    groupportMap.put(groupport.Group_Name__c,groupport);
                }
                
                
                
                List<AcctPlanCompanyPort__c> insertcompanyList = new List<AcctPlanCompanyPort__c>();
                for(String groupname : GroupCompanySequenceSet){
                    for(AcctPlanCompanyPort__c companyport : MapAccountwithgroup.get(groupname)) {
                        
                        
                        if(groupname != 'None'){
                            companyport.AcctPlanGroupPort__c = groupportMap.get(groupname).id;
                        }
                        companyport.Account_Plan_Portfolio__c = portfolio.id;
                        
                        insertcompanyList.add(companyport);
                        
                    }
                    
                    
                }
                insert insertcompanyList;
                
                PageReference pr = Page.AccountPlanPortfolioManagement;
                pr.setRedirect(true);
                pr.getParameters().put('id',portfolio.id);
                return pr;
                
                
            }catch(DMLException d){
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,d.getMessage()));
                return null;
            }
        }
        
        
        
        
        
        
    }
    
    
    
    
}*/