public class LeadValidation {}
/*
{
    public static List<UploadLead> Leadvalid(List<Object> valid, String Numbers)
    {
        string js =  JSON.serialize(valid);
        //system.debug('JSON : ' +js );
        List<UploadLead>  ret = (List<UploadLead>)JSON.deserialize(js, List<UploadLead>.class);
        //system.debug('Lead Name : ' +  ret);
        
        List<UploadLead> tempvalid = new List<UploadLead>();
        set<id> setaccId = new set<id>();
        set<String> setaccExId = new set<String>();
        set<id> setUserex = new set<id>();
        set<String> setUserName = new set<String>();
        set<String> setCampaignName = new set<String>();
        set<String> setIndustryName = new set<String>();
        set<String> setGroupName = new set<String>();
        Set<String> setBranch = new set<String>();
        Set<String> setIdNumber = new set<String>();
        set<String> s1 = new set<String>();
        set<Integer> s2 = new set<Integer>();
        
        for(UploadLead checkvalid : ret)
        {
            if(checkvalid.PrimaryCampaign != null)
            {
                String campaignname = checkvalid.PrimaryCampaign.trim();
                setCampaignName.add(campaignname);
            }
            if(checkvalid.Industry != null)
            {
                String Industryname = checkvalid.Industry.trim();
                setIndustryName.add(Industryname);
            }
            if(checkvalid.Groupname != null)
            {
                String Groupnames = checkvalid.Groupname.trim();
                setGroupName.add(Groupnames);
            }
            if(checkvalid.BranchedReferred != null)
            {
                if(Pattern.matches('^[0-9]{3}+$',checkvalid.BranchedReferred))
                {
                    setBranch.add(checkvalid.BranchedReferred);
                }
            }
            //system.debug('##checkvalid.LinktoCustomer  new ken = '+checkvalid.LinktoCustomer );
            if(checkvalid.LinktoCustomer != null)
            {
                if(checkvalid.LinktoCustomer.trim().length() == 30)
                {
                    setaccExId.add(checkvalid.LinktoCustomer.trim());
                }
            }
            if(checkvalid.IDNumber != null)
            {
                setIdNumber.add(checkvalid.IDNumber);
            }
            if(checkvalid.idtype != null && checkvalid.IDNumber != null && checkvalid.idtype != '' && checkvalid.IDNumber != '')
            {
					string typename = checkvalid.idtype.replaceAll('\\s+','');
                    //Tinnakrit Comment - Change To Map<String,String> do not compare with IDTYPE+IDNUMBER because ID TYPE has whitespace
                    //and make sure that program must be ignored Case Sensitives
                    if (s1.contains(typename.toLowerCase().trim()+checkvalid.IDNumber)) {
                        s2.add(checkvalid.row);
                    }else {
                        s1.add(typename.toLowerCase().trim()+checkvalid.IDNumber);
                    }
            }
        }
        
        
        
        //---------------------------------User ID---------------------------------//
        List<User> tempuser = [Select Id,IsActive,Employee_ID__c,name from user];
        Map<String,User> mapuserEx = new Map<String,User>();
        Map<String,Id> mapuserName = new Map<String,Id>();
        Map<String,Id> MapqueueName = new Map<String,Id>();
        
        Map<String,List<Id>> Mapcheckqueue = new Map<String,List<Id>>();
        List<Group> tempqueue = [Select Id,name,type from group where type =: 'Queue'];
        //system.debug('tempqueue = '+tempqueue);
        for(User usr: tempuser)
        {
            mapuserEx.put(usr.Employee_ID__c, usr);
            //mapuserName.put(usr.Name, usr.Id);
        }
        for(Group que :tempqueue)
        {
            MapqueueName.put(que.name, que.Id);
        }
        //System.debug('Check Mapcheckqueue 1 = '+Mapcheckqueue);
        
        //---------------------------------Custom Setting Error Message---------------------------------//
        Map<String,String> MapErrMsg = new Map<String,String>();
        List<Status_Code__c> errmsg = [Select Id,name,Status_Message__c from Status_Code__c];
        
        for(Status_Code__c er : errmsg) {
            MapErrMsg.put(er.name, er.Status_Message__c);
        }
        
        
        
        //---------------------------------Primary Campaign---------------------------------//
        List<Campaign> tempcampaign = [Select id,name,Isactive from Campaign where name IN: setCampaignName];
        Map<String,Campaign> mapCampaignName = new Map<String,Campaign>();
        if(tempcampaign.size() > 0)
        {
            for(Campaign tempcam : tempcampaign)
            {
                mapCampaignName.put(tempcam.name.tolowercase().trim(), tempcam);
            }
            //system.debug('##mapCampaignName = '+mapCampaignName);
        }
        
        //---------------------------------Industry---------------------------------//
        List<Industry__c> tempIndustry = [Select Id,name from Industry__c where name IN: setIndustryName];
        Map<String,Id> mapIndustryName = new Map<String,Id>();
        if(tempIndustry.size() > 0)
        {
            for(Industry__c tempin : tempIndustry)
            {
                mapIndustryName.put(tempin.name.tolowercase().trim(), tempin.Id);
            }
        }
        
        
        
        //---------------------------------Group Name---------------------------------//
        List<Group__c > tempGroup = [Select Id,name from Group__c where name IN: setGroupName];
        Map<String,Id> mapGroupName = new Map<String,Id>();
        if(tempGroup.size() > 0)
        {
            for(Group__c tempgro : tempGroup)
            {
                mapGroupName.put(tempgro.name.tolowercase().trim(), tempgro.Id);
            }
        }
        
        
        //---------------------------------Check Account ID---------------------------------//
        Map<String,Id> mapaccUserEx = new Map<String,Id>();
        List<account> tempacc = [Select Id,
                                 		name,
                                 		OwnerId,
                                 		TMB_Customer_ID_PE__c
                                 		//CRM_ID__c
                                 from Account 
                                 where TMB_Customer_ID_PE__c IN: setaccExId];
        //system.debug('##tempacc = '+tempacc);
        if(tempacc.size() > 0)
        {
            for(Account acc : tempacc)
            {
                mapaccUserEx.put(acc.TMB_Customer_ID_PE__c, acc.Id);
            }
        }
        
        
        
        //---------------------------------Check Dup Id Number Account---------------------------------//
        Map<String,Id> mapaccUserIdnumber = new Map<String,Id>();
        //List<account> tempIdnumber = [Select Id,
        //                         		name,
        //                         		OwnerId,
        //                         		CRM_ID__c,
        //                         		ID_Number_PE__c
        //                         from Account 
        //                         where ID_Number_PE__c  IN: setIdNumber];
        //FIND {setIdNumber} IN ID_Number_PE__c Fields RETURNING Account(Id);
        //List<List<SObject>> searchList = [FIND setIdNumber IN ID_Number_PE__c Fields RETURNING 
                                  //Account (Id)];
        
   
        
        
        
        
        
        //---------------------------------Branch and Zone---------------------------------//
        List<Branch_and_Zone__c> tempBranch = [Select Id,name,Branch_Code__c,Branch_Name__c from Branch_and_Zone__c where Branch_Code__c IN: setBranch];
        Map<String,Branch_and_Zone__c> mapBranch = new Map<String,Branch_and_Zone__c>();
        if(tempBranch.size() > 0 )
        {
            for(Branch_and_Zone__c tempBr : tempBranch)
            {
                mapBranch.put(tempBr.Branch_Code__c , tempBr);
            }
        }
        
        
        
        //---------------------------------Validation---------------------------------//
        
        //Tinnakrit Edit Jun 13 12:27 AM
        //
        //
        //
        //List<Group> sizequeue = [Select Id,name,type from group where type =: 'Queue' and  name =: checkvalid.leadOwner];
        //
        Set<String> QueueNameSet = new Set<String>();
        Map<String,List<Group>> queueNameMap = new Map<String,List<Group>>();
        for(UploadLead checkvalid : ret)
        {
            if(MapqueueName.get(checkvalid.leadOwner.trim()) != null && !Pattern.matches('^[0-9]{5}+$',checkvalid.leadOwner)){
                QueueNameSet.add(checkvalid.leadOwner);
            }
        }
        
        for(Group queueValue : [Select Id,name,type from group where type =: 'Queue' and  name IN: QueueNameSet]){
            if(queueNameMap.containsKey(queueValue.name)){
                queueNameMap.get(queueValue.name).add(queueValue);
            }else{
                List<Group> groupList = new List<group>();
                groupList.add(queueValue);
                queueNameMap.put(queueValue.name,groupList);
            }
        }
        
        
        //
        //
        //Tinnakrit Edit Jun 13 12:27 AM
        
        
        for(UploadLead checkvalid : ret)
        {
            checkvalid.errormessage = '';
            

            if(checkvalid.LinktoCustomer.trim().length() != 0)
            {
                    if(checkvalid.LinktoCustomer.trim().length() == 30)
                    {
                     	if(mapaccUserEx.keySet().contains(checkvalid.LinktoCustomer.trim()) == true && mapaccUserEx.get(checkvalid.LinktoCustomer.trim()) != null )
                        { 
                          checkvalid.LinktoCustomer = mapaccUserEx.get(checkvalid.LinktoCustomer.trim());
                          checkvalid.flag = True;   
                        }else{
                        checkvalid.flag = False;
                        checkvalid.errormessage += MapErrMsg.get('5014')+', \r\n';    
                    	}
                    }
              //  system.debug('##checkvalid.LinktoCustomer new = '+checkvalid.LinktoCustomer);
            }
           
            if(string.isBlank(checkvalid.CustomerType)) {
                checkvalid.errormessage += MapErrMsg.get('5015')+', \r\n'; 
                checkvalid.flag = False;
            }
            
            if(string.isBlank(checkvalid.CustomerName)) {
                checkvalid.errormessage += MapErrMsg.get('5016')+', \r\n';
                checkvalid.flag = False;
            }
            
            if(string.isBlank(checkvalid.ContactLastname)) {
                checkvalid.errormessage += MapErrMsg.get('5017')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.IDNumber != null && checkvalid.IDNumber.length() > 25) {
                checkvalid.errormessage += MapErrMsg.get('5024')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.Address != null && checkvalid.Address.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5025')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.SubDistrict != null && checkvalid.SubDistrict.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5026')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.District != null && checkvalid.District.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5027')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.Email != null && checkvalid.Email.length() > 80) {
                checkvalid.errormessage += MapErrMsg.get('5028')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.Ext != null && checkvalid.Ext.length() > 10) {
                checkvalid.errormessage += MapErrMsg.get('5029')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ZipCode != null && checkvalid.ZipCode.length() > 10) {
                checkvalid.errormessage += MapErrMsg.get('5030')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ReferralStaffID != null && checkvalid.ReferralStaffID.length() > 5) {
                checkvalid.errormessage += MapErrMsg.get('5031')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ReferralStaffName != null && checkvalid.ReferralStaffName.length() > 100) {
                checkvalid.errormessage += MapErrMsg.get('5032')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.customerName != null && checkvalid.customerName.length() > 255) {
                checkvalid.errormessage += MapErrMsg.get('5034')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.customerNameEN != null && checkvalid.customerNameEN.length() > 100) {
                checkvalid.errormessage += MapErrMsg.get('5035')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ContactPhoneNumber != null && checkvalid.ContactPhoneNumber.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5036')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.MobileNo != null && checkvalid.MobileNo.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5037')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.OfficeNo != null && checkvalid.OfficeNo.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5038')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ContactPosition != null && checkvalid.ContactPosition.length() > 128) {
                checkvalid.errormessage += MapErrMsg.get('5039')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ContactFirstname != null && checkvalid.ContactFirstname.length() > 40) {
                checkvalid.errormessage += MapErrMsg.get('5040')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.ContactLastname != null && checkvalid.ContactLastname.length() > 80) {
                checkvalid.errormessage += MapErrMsg.get('5046')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.NoOfyears != null && checkvalid.NoOfyears != ''){
                if(checkvalid.NoOfyears.length() > 5 || !checkvalid.NoOfyears.isNumeric()) {
                checkvalid.errormessage += MapErrMsg.get('5044')+', \r\n';
                checkvalid.flag = False;
                }
            }
            if(checkvalid.OtherSource != null && checkvalid.OtherSource.length() > 25) {
                checkvalid.errormessage += MapErrMsg.get('5045')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.Remark != null && checkvalid.Remark.length() > 255) {
                checkvalid.errormessage += MapErrMsg.get('5047')+', \r\n';
                checkvalid.flag = False;
            }
            if((checkvalid.prescreen != null && checkvalid.prescreen != '') && checkvalid.prescreen.tolowercase().trim() != 'passed') {
                checkvalid.errormessage += MapErrMsg.get('5048')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.SalesAmountperyear != null && checkvalid.SalesAmountperyear != ''){
            	List<String> SalesAmount = checkvalid.SalesAmountperyear.trim().split('\\.');
                if(SalesAmount.size() == 1) {
                    if(SalesAmount[0] != null && SalesAmount[0].length() > 13) {
                        checkvalid.errormessage += MapErrMsg.get('5041')+', \r\n';
                        checkvalid.flag = False;
                    }
                }else if(SalesAmount.size() == 2) {
                    if(SalesAmount[1] != null && SalesAmount[1].length() > 2) {
                        checkvalid.errormessage += MapErrMsg.get('5041')+', \r\n';
                        checkvalid.flag = False;
                    }
                }
            }
            if(checkvalid.TotalExpectedRevenue != null && checkvalid.TotalExpectedRevenue != ''){
                List<String> TotalExpected = checkvalid.TotalExpectedRevenue.trim().split('\\.');
                if(TotalExpected.size() == 1) {
                    if(TotalExpected[0] != null && TotalExpected[0].length() > 16) {
                        checkvalid.errormessage += MapErrMsg.get('5042')+', \r\n';
                        checkvalid.flag = False;
                    }
                }else if(TotalExpected.size() == 2) {
                    if(TotalExpected[1] != null && TotalExpected[1].length() > 2) {
                        checkvalid.errormessage += MapErrMsg.get('5042')+', \r\n';
                        checkvalid.flag = False;
                    }
                }
            }
            if(checkvalid.leadOwner != null && checkvalid.leadOwner != '')
            {
                
                //System.debug('Check Mapcheckqueue 2 = '+Mapcheckqueue.get(checkvalid.leadOwner.trim()));
                if(Pattern.matches('^[0-9]{5}+$',checkvalid.leadOwner))
                {
                    if(mapuserEx.containsKey(checkvalid.leadOwner))
                    {
                        if( mapuserEx.get(checkvalid.leadOwner).isActive == true) {
                            checkvalid.leadOwner = mapuserEx.get(checkvalid.leadOwner).Id;
                            if(checkvalid.flag != False){
                                checkvalid.flag = True;
                            }
                        }else if(mapuserEx.get(checkvalid.leadOwner).isActive == false ) {
                            checkvalid.errormessage += MapErrMsg.get('5033')+', \r\n'; 
                       		checkvalid.flag = False; 
                        }
                    }
                    else
                    {
                       checkvalid.errormessage += MapErrMsg.get('5019')+', \r\n'; 
                       checkvalid.flag = False; 
                    }
                }
               // else if(MapqueueName.get(checkvalid.leadOwner.trim()) != null)
                  else if(queueNameMap.containsKey(checkvalid.leadOwner.trim()))
                {
                    List<group> sizequeue = queueNameMap.get(checkvalid.leadOwner.trim());
                   // List<Group> sizequeue = [Select Id,name,type from group where type =: 'Queue' and  name =: checkvalid.leadOwner];
                    if(sizequeue.size() > 1){
                        checkvalid.errormessage += MapErrMsg.get('5004')+' \r\n'; 
                        checkvalid.flag = False;
                    }else{
                        checkvalid.leadOwner = MapqueueName.get(checkvalid.leadOwner.trim());
                        if(checkvalid.flag != False){
                        	checkvalid.flag = True;
                        }
                    }
                }
                else
                {
                   checkvalid.errormessage += MapErrMsg.get('5019')+', \r\n'; 
                   checkvalid.flag = False; 
                }
            }
            else
            {
                checkvalid.leadOwner = UserInfo.getUserId();
                if(checkvalid.flag != False){
                   checkvalid.flag = True;
                }  
            }
            if(s2.contains(checkvalid.row))
            {
                    checkvalid.Errormessage += MapErrMsg.get('5003')+', \r\n';
                    checkvalid.flag = false;
            }
            if(checkvalid.PrimaryCampaign != null && checkvalid.PrimaryCampaign != '')
            {
                if(mapCampaignName.get(checkvalid.PrimaryCampaign.tolowercase().trim()) != null)
                {
                    //if(mapCampaignName.get(checkvalid.PrimaryCampaign.tolowercase().trim()).isActive == true) {   
                        checkvalid.PrimaryCampaign = mapCampaignName.get(checkvalid.PrimaryCampaign.tolowercase().trim()).id;
                       // system.debug('checkvalid.flag = '+checkvalid.flag);
                        if(checkvalid.flag != false){
                            checkvalid.flag = True; 
                        }
                }
                else
                {
                    checkvalid.errormessage += MapErrMsg.get('5005')+', \r\n'; 
                    checkvalid.flag = False;
                    //system.debug('##checkvalid.errormessage = '+checkvalid.errormessage);
                    //system.debug('##checkvalid.flag(campaign) = '+checkvalid.flag);
                }
            }
            if(checkvalid.Industry != null && checkvalid.Industry != '' && checkvalid.Industry.length() != 0)
            {
                if(mapIndustryName.get(checkvalid.Industry.tolowercase().trim()) != null)
                {
                   checkvalid.Industry = mapIndustryName.get(checkvalid.Industry.tolowercase().trim());
                   // system.debug('##mapIndustryName.Industry = '+mapIndustryName.get(checkvalid.Industry.tolowercase().trim()));
                    if(checkvalid.flag != false){
                        checkvalid.flag = True;
                    }
                }
                else if(mapIndustryName.get(checkvalid.Industry.tolowercase().trim()) == null)
                {
                    checkvalid.errormessage += MapErrMsg.get('5006')+', \r\n'; 
                    checkvalid.flag = False;
                }
            }
            if(checkvalid.Groupname != null && checkvalid.Groupname != '')
            {
                if(mapGroupname.get(checkvalid.Groupname.tolowercase().trim()) != null)
                {
                   checkvalid.Groupname = mapGroupname.get(checkvalid.Groupname.tolowercase().trim());
                    if(checkvalid.flag != false){
                        checkvalid.flag = True;  
                    }
                }
                else if(mapGroupname.get(checkvalid.Groupname.tolowercase().trim()) == null)
                {
                    checkvalid.errormessage += MapErrMsg.get('5007')+', \r\n'; 
                    checkvalid.flag = False;
                }
            }
            if(checkvalid.BranchedReferred.length() != 0)
            {
                //system.debug('##checkvalid.BranchedReferred.length() = '+checkvalid.BranchedReferred.length());
                if(Pattern.matches('^[0-9]{3}+$',checkvalid.BranchedReferred) || Pattern.matches('^[0-9]{4}+$',checkvalid.BranchedReferred ))
                {
                    //system.debug('(mapBranch.get(checkvalid.BranchedReferred) = '+mapBranch.get(checkvalid.BranchedReferred).Id);
                    //system.debug('mapBranch.get(checkvalid.BranchedReferred).Branch_Name__c = '+mapBranch.get(checkvalid.BranchedReferred).Branch_Name__c);
                    //system.debug('## validation BranchedReferredName : '+checkvalid.BranchedReferredName);
                    if(mapBranch.get(checkvalid.BranchedReferred) != null)
                    {
                        checkvalid.BranchedReferred = mapBranch.get(checkvalid.BranchedReferred).Id;
                        checkvalid.BranchedReferredName = mapBranch.get(checkvalid.BranchedReferredName).Branch_Name__c;
                        if(checkvalid.flag != false){
                            checkvalid.flag = True;  
                        }
                    }
                    else
                    {
                        checkvalid.errormessage += MapErrMsg.get('5008')+', \r\n'; 
                        checkvalid.flag = False;
                    }
                }
                else
                {
                    checkvalid.errormessage += MapErrMsg.get('5009')+', \r\n'; 
                    checkvalid.flag = False;
                }
            }
            if(checkvalid.CustomerType == 'Juristic' && (checkvalid.IDType != null && checkvalid.IDType != '')&& checkvalid.IDType != 'BRN ID' ){
                checkvalid.errormessage += MapErrMsg.get('5010')+', \r\n';
                checkvalid.flag = False;
            }
            if(checkvalid.CustomerType == 'Individual' && checkvalid.IDType == 'BRN ID') {
                checkvalid.errormessage += MapErrMsg.get('5018')+', \r\n';
                checkvalid.flag = False;  
            }
            if(checkvalid.ContactPhoneNumber != '' && checkvalid.ContactPhoneNumber != null) {
                if(!Pattern.matches('[\\d\\-\\s]*',checkvalid.ContactPhoneNumber)) {
                   checkvalid.errormessage += MapErrMsg.get('5021')+', \r\n'; 
                   checkvalid.flag = False; 
                }
            }
            if(checkvalid.OfficeNo != '' && checkvalid.OfficeNo != null) {
                if(!Pattern.matches('[\\d\\-\\s]*',checkvalid.OfficeNo)) {
                   checkvalid.errormessage += MapErrMsg.get('5022')+', \r\n'; 
                   checkvalid.flag = False; 
                }
            }
            if(checkvalid.MobileNo != '' && checkvalid.MobileNo != null) {
                if(!Pattern.matches('[\\d\\-\\s]*',checkvalid.MobileNo)) {
                   checkvalid.errormessage += MapErrMsg.get('5023')+', \r\n'; 
                   checkvalid.flag = False; 
                }
            }
            if(checkvalid.CustomerType == 'Individual' && checkvalid.IDType == 'Citizen ID' && checkvalid.IdNumber != '')
            {
                if(checkvalid.IDType == 'Citizen ID' && !Pattern.matches('^[0-9]{13}+$',checkvalid.IdNumber)){         
                    checkvalid.errormessage += MapErrMsg.get('5011')+', \r\n';
                    checkvalid.flag = False;    
                }else if(checkvalid.IDType == 'Citizen ID' && Pattern.matches('^[0-9]{13}+$',checkvalid.IdNumber)){
                    string[] citizen = checkvalid.IdNumber.split('');
                   // system.debug('citizen = '+citizen);
                    integer sum = 0; 
                    for(integer i = 0; i < 12; i++) { 
                        sum += Integer.valueof(citizen[i])*(13-i);
                    }
                   // system.debug('sum is : '+sum);
                    //system.debug('math.mod(11-(math.mod(s,11))) = '+11-math.mod(sum,11);
                  //  system.debug('math.mod(11-(math.mod(s,11)),10) = '+math.mod(11-(math.mod(sum,11)),10));
                   // system.debug('integer.valueOf(citizen[12]) = '+integer.valueOf(citizen[12]));
                    if(math.mod(11-math.mod(sum,11),10) != Integer.valueof(citizen[12])){
                        checkvalid.errormessage += MapErrMsg.get('5011')+', \r\n';
                        checkvalid.flag = False;
                    }else{
                        if(checkvalid.flag != false){
                        checkvalid.flag = True;
                        }else{
                            checkvalid.flag = False;
                        }
                    }
                }
            }
            if(checkvalid.Email != null && checkvalid.Email != '' && !Pattern.matches('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$',checkvalid.Email)){
               checkvalid.errormessage += MapErrMsg.get('5012')+', \r\n';
               checkvalid.flag = False;
            }
			//checkvalid.errormessage = checkvalid.errormessage.subString(0,checkvalid.errormessage.Length()-2);
            //system.debug(checkvalid.errormessage);
            tempvalid.add(checkvalid);
        }
        
        
        
        //system.debug('##tempvalid = '+tempvalid);
        return tempvalid;
    }   
    
}*/