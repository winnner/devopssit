public class SmartBDM_Utility {

    private final static Map<String, String> QuickCA_Lead_Stage = new Map<String, String> { 
        '0' => 'NewLead', 
        '1' => 'Contact', 
        '2' => 'AddProduct', 
        '3' => 'Convert', 
        '4' => 'SubmitSLS' 
    };

    private final static Map<String, String> QuickCA_Opportunity_Stage = new Map<String, String> { 
        '0' => 'NewOpportunity', 
        '1' => 'Contact', 
        '2' => 'AddProduct', 
        '3' => 'SubmitSLS'
    };

    @AuraEnabled
    public static void updateLeadStatus(String leadId, String stage)
    {
        try
        {
            Lead ld = new Lead(Id = leadId);
            ld.SmartBDM_QuickCA_ProcessStatus__c = QuickCA_Lead_Stage.get(stage);
            update ld;
        }
        catch(Exception e) 
        {
            System.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());           
        }   
    }
    
    @AuraEnabled
    public static void updateOpptyStatus(String opptyId,String stage)
    {
        try
        {
            Opportunity oppty = new Opportunity(Id = opptyId);
            oppty.SmartBDM_QuickCA_ProcessStatus__c = QuickCA_Opportunity_Stage.get(stage);
            update oppty;
        }
        catch(Exception e) 
        {
            System.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());           
        }   
    }

    public static String tokenKeyCalculateTools {
        get{
            return (String)orgPartCreateToken('SmartBDMCalToolsCreateToken', 'Smart_BDM_Cal_Tools_Create_Token').get('accesstoken');
        }
    }

    // V1 - Token Gateway for calculate tools only
    public static Cache.OrgPartition orgPartCreateToken(string partitionName, string namedCredentials) {
        Cache.OrgPartition orgPartCreateToken = Cache.Org.getPartition('local.' + partitionName);
        if(orgPartCreateToken.get('accesstoken') == null || orgPartCreateToken.get('accesstoken') == '') {
            Datetime startTime = Datetime.now();
            AuthenticationToken authenTokenCalculateTools = SmartBDM_Service.startCallGetTokenService(namedCredentials);
            Datetime endTime = Datetime.now();
            orgPartCreateToken.put('accesstoken', authenTokenCalculateTools.access_token, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('tokentype', authenTokenCalculateTools.token_type, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('expiresin', authenTokenCalculateTools.expires_in, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('scope', authenTokenCalculateTools.scope, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('jti', authenTokenCalculateTools.jti, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('clientid', authenTokenCalculateTools.client_id, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('version', authenTokenCalculateTools.version, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            
            OnlineServiceLog onlineServiceLogGateWay = new OnlineServiceLog(authenTokenCalculateTools.isSuccess, authenTokenCalculateTools.errorMessage, authenTokenCalculateTools.requestBody, authenTokenCalculateTools.responseBody, startTime, endTime);
            orgPartCreateToken.put('onlineServiceLogGateWay', onlineServiceLogGateWay.parseToJson(), Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
        }
        return orgPartCreateToken;
    }

    // V1 - Token Gateway for calculate tools only
    // V2 - Token Gateway for mapping key another project
    public static Cache.OrgPartition orgPartCreateToken(string partitionName, string namedCredentials, string projectKey) {
        Cache.OrgPartition orgPartCreateToken = Cache.Org.getPartition('local.' + partitionName);
        if(orgPartCreateToken.get(projectKey + 'accesstoken') == null || orgPartCreateToken.get(projectKey + 'accesstoken') == '') {
            Datetime startTime = Datetime.now();
            AuthenticationToken authenTokenCalculateTools = SmartBDM_Service.startCallGetTokenService(namedCredentials);
            Datetime endTime = Datetime.now();
            orgPartCreateToken.put(projectKey + 'accesstoken', authenTokenCalculateTools.access_token, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'tokentype', authenTokenCalculateTools.token_type, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'expiresin', authenTokenCalculateTools.expires_in, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'scope', authenTokenCalculateTools.scope, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'jti', authenTokenCalculateTools.jti, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'clientid', authenTokenCalculateTools.client_id, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put(projectKey + 'version', authenTokenCalculateTools.version, Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            
            OnlineServiceLog onlineServiceLogGateWay = new OnlineServiceLog(authenTokenCalculateTools.isSuccess, authenTokenCalculateTools.errorMessage, authenTokenCalculateTools.requestBody, authenTokenCalculateTools.responseBody, startTime, endTime);
            orgPartCreateToken.put(projectKey + 'onlineServiceLogGateWay', onlineServiceLogGateWay.parseToJson(), Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
        }
        return orgPartCreateToken;
    }

    public static void orgPartCreateOnlineServiceLog(string partitionName, String onlineServiceCacheName, OnlineServiceLog onlineServiceLog) {
        Cache.Org.getPartition('local.' + partitionName).put(onlineServiceCacheName, onlineServiceLog.parseToJson(), Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
    }

    public static OnlineServiceLog onlineServiceLog(String partitionName, String onlineServiceCacheName) {
        return Cache.Org.getPartition('local.' + partitionName).get(onlineServiceCacheName) != null ? (OnlineServiceLog)JSON.deserialize((String)Cache.Org.getPartition('local.' + partitionName).get(onlineServiceCacheName), OnlineServiceLog.class) : null;
    }

    private final static String SmartBDMEnliteTokenGateWay = 'SmartBDMEnlite';
    public static String tokenKeyEnliteGateWay {
        get{
            // namedCredentials use gate way of calculate tools
            return (String)orgPartCreateToken('CreateGateWayToken', 'Smart_BDM_Cal_Tools_Create_Token', SmartBDMEnliteTokenGateWay).get(SmartBDMEnliteTokenGateWay + 'accesstoken');
        }
    }

    private final static String SmartBDMEnliteToken = 'SmartBDMEnliteToken';
    public static Cache.OrgPartition orgPartCreateTokenEnlite(string partitionName) {
        Cache.OrgPartition orgPartCreateToken = Cache.Org.getPartition('local.' + partitionName);
        if(orgPartCreateToken.get(SmartBDMEnliteToken) == null || orgPartCreateToken.get(SmartBDMEnliteToken) == '') {
            SmartBDM_EnliteService.SmartBDMEnliteTokenWrapper authenTokenEnliteWrapper = SmartBDM_EnliteService.startCallGetTokenService(new SmartBDMEnliteToken.Request(Smart_BDM_Constant.TOKEN_REQUEST_BODY_ENLITE.get('ClientName'), Smart_BDM_Constant.TOKEN_REQUEST_BODY_ENLITE.get('UserName')));
            orgPartCreateToken.put(SmartBDMEnliteToken, authenTokenEnliteWrapper.enliteToken.response.GetTokenResponse != null ? authenTokenEnliteWrapper.enliteToken.response.GetTokenResponse.TokenKey : '', Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
            orgPartCreateToken.put('onlineServiceLogEnliteToken', authenTokenEnliteWrapper.onlineServiceLog.parseToJson(), Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
        }
        return orgPartCreateToken;
    }
    public static String tokenKeyEnliteRequestBody {
        get{
            return (String)orgPartCreateTokenEnlite('CreateGateWayToken').get(SmartBDMEnliteToken);
        }
    }

    public static void orgPartClearValue(string partitionName, string keyOrgPartition) {
        Cache.Org.getPartition('local.' + partitionName).put(keyOrgPartition, '', Smart_BDM_Constant.ttlSecs, Cache.Visibility.ALL, false);
    }
}