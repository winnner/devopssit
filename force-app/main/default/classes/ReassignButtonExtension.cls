public class ReassignButtonExtension {}
    /*Comment Clean Code
    public Account acc;
    public ApexPages.StandardController std;
    public String strCallAlert{        
        get;set;
    }
    public ReassignButtonExtension(ApexPages.StandardController controller){
        acc = (Account)controller.getRecord();
        //oppInput = [ select Id,Name,StageName,AccountId,Application_Status__c from Account where Id =: oppInput.Id ];
        acc = [ select Id,Name,Account_Type__c,Suggested_Sub_Segment__c,OwnerId,Owner_change_notification__c 
               from Account 
               where Id =: acc.Id ];
        std = controller;
    }
    
    
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_UPDATE = 'update';
    static String strResult = '';
    
    //public static void Trigger_T01(List<Account> accsNew,List<Account> accsOld,String eventMode){
    public void Trigger_T01(){
        System.debug(':::: Trigger_T01 Start ::::');
        Map<String,String> listSuggestedSubSegment = new Map<String,String>();
        List<Prospect_Owner_Assignment__c> listTMBStaffID =  Prospect_Owner_Assignment__c.getall().values();    
        List<User> listUser = [select Id,Employee_ID__c from User];
        Map<String,String> listEmployeeId = new Map<String,String>();*/
       /* List<Account> listAccNew = new List<Account>();
        Map<Id,Account> listAccsOld = new Map<Id,Account>();
        if( eventMode == STR_UPDATE ){
        	listAccsOld.putAll(accsOld);    
        }
        */
        /*Comment Clean Code
        for (Prospect_Owner_Assignment__c poa : listTMBStaffID){
            if(poa.Name != null && poa.TMB_Staff_ID__c != null){
            	listSuggestedSubSegment.put(poa.Name, poa.TMB_Staff_ID__c);
            }
        }
        
        for (User u : listUser){
            if(u.Employee_ID__c != null){
                listEmployeeId.put(u.Employee_ID__c, u.Id);
            }
            
        }
        
        //System.debug(':: '+listSuggestedSubSegment);
        
     // for(Account acc : oppInput){
            if( ( acc.Account_Type__c == 'Prospect' || acc.Account_Type__c == 'Qualified Prospect' )
               && acc.Suggested_Sub_Segment__c != null){
                   detectError = false;
                   errorException = '';
                   String tmbId = '';
                   Id userId;
                   
                   //System.debug('::::: '+acc.Id +' | '+acc.Name + ' | '+acc.Suggested_Sub_Segment__c);
                   //System.debug('::::: detail input : '+acc);
                   if( listSuggestedSubSegment.containsKey(acc.Suggested_Sub_Segment__c) ){
                       tmbId = listSuggestedSubSegment.get(acc.Suggested_Sub_Segment__c);
                   }else{
                       detectError = true;
                       errorException = 'error containsKey Suggested_Sub_Segment__c : '+acc.Suggested_Sub_Segment__c;
                       acc.Re_assign_prospect__c = 'No';
                       acc.addError( Trigger_Msg__c.getValues('Not_Found_Suggested_Segment').Description__c  ,false); 
                       strResult = Trigger_Msg__c.getValues('Not_Found_Suggested_Segment').Description__c;
                   } 
                   
                   if( listEmployeeId.containsKey(tmbId) ){
                       userId = listEmployeeId.get(tmbId);
                       System.debug('tmbId '+tmbId);
                       System.debug('userId '+userId);
                   }else{
                       detectError = true;
                       errorException = 'error containsKey tmbId : '+tmbId;
                   }
                   
                     if( !detectError ){
                         try {
                            acc.OwnerId = userId;
                       		acc.Owner_change_notification__c = true; 
                             update acc;
                             strResult = Trigger_Msg__c.getValues('Reassign_Prospect').Description__c;
                              System.debug('::::: Account Id : '+acc.id +' : Owner Change !! :::::');
                       //System.debug('::::: Account Id Old : '+acc.id +' : '+listAccsOld.get(acc.Id).OwnerId+' , '+listAccsOld.get(acc.Id).Owner_change_notification__c+' :::::');
                       System.debug('::::: Account Id New : '+acc.id +' : '+acc.OwnerId+' , '+acc.Owner_change_notification__c+' :::::');
                         }  
                         catch(DmlException e){
                			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
           				 }       
                       //listAccNew.add(acc);    
                      
                   }else{
                       System.debug('::::: Account Id : '+acc.id +' : '+errorException+' :::::');
                   }
                   
                    strCallAlert = '<script>checkResult(\'' +strResult+ '\');</script>';
                   
        
                   
               } 
        if (acc.Suggested_Sub_Segment__c == null){
            strResult = Trigger_Msg__c.getValues('Not_Found_Segment').Description__c;
            strCallAlert = '<script>checkResult(\'' +strResult+ '\');</script>';
        }
       // }
                //if(listAccNew.size() > 0){
        // update listAccNew; // <===   Not Do this !!  if we want to update in same object in trigger we not update  list like this . Force.com will take care for us
        // }
        //System.debug('::::: List for update '+listAccNew.size()+' row :::::');
        System.debug(':::: Trigger_T01 End ::::');
    }

}*/