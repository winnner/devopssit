public with sharing class LogACallUnderAccountProxy {
   public sObject obj;
    private ApexPages.StandardController standardController;    
    public LogACallUnderAccountProxy(ApexPages.StandardController controller){
        standardController = controller;        
        obj = (SObject)standardController.getRecord();
    }     
    public  PageReference NextPage(){   
    system.debug(obj.getSObjectType());
    system.debug(obj.Id);
        if(String.valueof(obj.getSObjectType()) =='Contact'){
            PageReference result = Page.LogACallMobileLayout;
            Contact Con = [SELECT ID,AccountID from Contact WHERE ID =: obj.id LIMIT 1];
            result.getParameters().put('what_id',Con.AccountID);
            result.getParameters().put('who_id',Con.Id);
            result.setRedirect(true); 
       return result;  
                  
        }else if(String.valueof(obj.getSObjectType()) =='Account'){
            PageReference result2 =Page.LogACallMobileLayout;
            result2.getParameters().put('what_id',obj.Id);
            result2.setRedirect(true); 
       return result2;  
        }else  if(String.valueof(obj.getSObjectType()) =='Opportunity'){
            PageReference result3 =Page.LogACallMobileLayout;
            result3.getParameters().put('what_id',obj.Id);
            result3.setRedirect(true); 
       return result3;  
        }else if(String.valueof(obj.getSObjectType()) == 'Lead'){
            PageReference result4 =Page.LogACallMobileLayout;
            result4.getParameters().put('who_id',obj.Id);
            result4.setRedirect(true); 
       return result4; 
        }
       return null;
    }
}