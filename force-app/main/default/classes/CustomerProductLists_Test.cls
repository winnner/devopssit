@isTest
public without sharing class CustomerProductLists_Test {

    // static fnaProductDetailCtrl testClass;
    static CustomerProductLists customerProdList;

    @isTest
    private static void Test_CustomerProductLists() {
        TestUtils.createAppConfig();

        List<Account> listAccount = new List<Account>();
        Account account = new Account();
        account.Name = 'Mockup Data';
        account.RTL_Office_Phone_Number__c = '012345678';
        account.Mobile_Number_PE__c = '0';
        account.TMB_Customer_ID_PE__c = '001100000000000000000014144590';
        listAccount.add(account);
        insert listAccount;

        List<FNA_Activity__c> listFnaAct = new List<FNA_Activity__c>();
        FNA_Activity__c fnaActivity = new FNA_Activity__c();
        fnaActivity.Customer__c = listAccount[0].Id;
        listFnaAct.add(fnaActivity);
        insert listFnaAct;

        List<FNA_Product_Offering__c> listProductOffering = new List<FNA_Product_Offering__c>();
        FNA_Product_Offering__c productOffering = new FNA_Product_Offering__c();
        productOffering.Flag_Offset_product_holding__c = true;
        // productOffering.FNA_Activity__c = listFnaAct[0].Id;
        listProductOffering.add(productOffering);
        insert listProductOffering;

        System.currentPageReference().getParameters().put('itemNumber', '1');
        System.currentPageReference().getParameters().put('accountId', listAccount[0].Id);
        System.currentPageReference().getParameters().put('productType', 'DepositLoan');
        System.currentPageReference().getParameters().put('tabindex', '0');

        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": [{"accountTitle": "นาง ทด สอบ","accountNumber": "2471779484","accountType": "DDA","openDate": "2013-12-13","accountStatus": "Active | ปกติ (Active)","productCode": "101","smsAlertEligible": "0","smsAlertRegistered": "0"}],"creditCards": [{"accountTitle": "","accountNumber": "496694006242032105","accountType": "CCA","openDate": "2008-11-24","accountStatus": "BLCK B","productCode": "011"}],"loans": [{"accountTitle": "NAME TEST","accountNumber": "00015405889701","accountType": "LOC","openDate": "9999-12-31","accountStatus": "Active","productCode": "S700"}],"bancassurances": [{"accountTitle": "BLANK","accountNumber": "00000000051187","accountType": "BA","accountStatus": "Active","productCode": ""}],"investments": [{"accountTitle": "BLANK","accountNumber": "PT000000000000000003","accountType": "MFA","accountStatus": "Active | ปกติ (Active)","productCode": "SAS"}],"treasuries": [],"tradeFinances": []}}';
        String responseBodyOSC16 = '{"status": {"code": "0000","description": "Success"},"bancassurances": null,"total": "0"}';

        customerProdList = new CustomerProductLists();

        customerProdList.fnaActivityId = listFnaAct[0].Id;
        customerProdList.caseProductBancassurance = null;
        customerProdList.caseProductInvestment = null;
        customerProdList.productBAs = null;
        customerProdList.productInvestment = null;
        customerProdList.productAccounts = CaseAccountProduct.parseJsonToObj(responseBodyOSC14);
        customerProdList.searchProduct1 = 'test';
        customerProdList.searchProduct2 = 'test';
        customerProdList.searchProduct3 = 'test';

        customerProdList.setupCustomerList();
        customerProdList.startCallCaseProductDeposit();
        customerProdList.processResponseWsOSC14();
        customerProdList.startCallCaseProductInvestment();
        customerProdList.processResponseWsOSC15();
        customerProdList.startCallCaseProductBA();
        customerProdList.processResponseWsOSC16();
        customerProdList.getcaseProductDepositAndLoanList();

        customerProdList.searchProductNumber();
        customerProdList.clearSearch();
        customerProdList.setPaginationObj();
        customerProdList.nextBA();
        customerProdList.nextInvestment();
        // customerProdList.previousDepositAndLoan(); //List index out of bound -10
        // customerProdList.previousBA(); //List index out of bound -10
        // customerProdList.previousInvestment(); //List index out of bound -10
        String strData = 'TEST';
        String strFind = 'TEST';
        Boolean containResult = CustomerProductLists.containsIgnoreCase(strData,strFind);

    }

    @isTest
    private static void Test_searchProductNumber_DepositLoan() {
        // customerProdList = new CustomerProductLists();
        // customerProdList.clearSearch();
    }

    @isTest
    private static void Test_searchProductNumber_Bancassurance() {
        System.currentPageReference().getParameters().put('productType', 'Bancassurance');
        customerProdList = new CustomerProductLists();
        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": [{"accountTitle": "นาง ทด สอบ","accountNumber": "2471779484","accountType": "DDA","openDate": "2013-12-13","accountStatus": "Active | ปกติ (Active)","productCode": "101","smsAlertEligible": "0","smsAlertRegistered": "0"}],"creditCards": [{"accountTitle": "","accountNumber": "496694006242032105","accountType": "CCA","openDate": "2008-11-24","accountStatus": "BLCK B","productCode": "011"}],"loans": [{"accountTitle": "NAME TEST","accountNumber": "00015405889701","accountType": "LOC","openDate": "9999-12-31","accountStatus": "Active","productCode": "S700"}],"bancassurances": [{"accountTitle": "BLANK","accountNumber": "00000000051187","accountType": "BA","accountStatus": "Active","productCode": ""}],"investments": [{"accountTitle": "BLANK","accountNumber": "PT000000000000000003","accountType": "MFA","accountStatus": "Active | ปกติ (Active)","productCode": "SAS"}],"treasuries": [],"tradeFinances": []}}';
        customerProdList.productAccounts = CaseAccountProduct.parseJsonToObj(responseBodyOSC14);
        customerProdList.searchProductNumber();
        customerProdList.clearSearch();
    }

    @isTest
    private static void Test_searchProductNumber_Investment() {
        System.currentPageReference().getParameters().put('productType', 'Investment');
        customerProdList = new CustomerProductLists();
        String responseBodyOSC14 = '{"status": {"code": "0000","description": "Success"},"accounts": {"deposits": [{"accountTitle": "นาง ทด สอบ","accountNumber": "2471779484","accountType": "DDA","openDate": "2013-12-13","accountStatus": "Active | ปกติ (Active)","productCode": "101","smsAlertEligible": "0","smsAlertRegistered": "0"}],"creditCards": [{"accountTitle": "","accountNumber": "496694006242032105","accountType": "CCA","openDate": "2008-11-24","accountStatus": "BLCK B","productCode": "011"}],"loans": [{"accountTitle": "NAME TEST","accountNumber": "00015405889701","accountType": "LOC","openDate": "9999-12-31","accountStatus": "Active","productCode": "S700"}],"bancassurances": [{"accountTitle": "BLANK","accountNumber": "00000000051187","accountType": "BA","accountStatus": "Active","productCode": ""}],"investments": [{"accountTitle": "BLANK","accountNumber": "PT000000000000000003","accountType": "MFA","accountStatus": "Active | ปกติ (Active)","productCode": "SAS"}],"treasuries": [],"tradeFinances": []}}';
        customerProdList.productAccounts = CaseAccountProduct.parseJsonToObj(responseBodyOSC14);
        customerProdList.searchProductNumber();
        customerProdList.clearSearch();
    }

    @isTest
    private static void Test_setPaginationObj() {
        System.currentPageReference().getParameters().put('tabindex', '0');
        customerProdList = new CustomerProductLists();
        customerProdList.setPaginationObj();
    }

    @isTest
    private static void Test_setPaginationObj_1() {
        System.currentPageReference().getParameters().put('tabindex', '-1');
        customerProdList = new CustomerProductLists();
        customerProdList.setPaginationObj();
    }
}