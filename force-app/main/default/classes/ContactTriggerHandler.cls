public class ContactTriggerHandler {
    
    static Boolean detectError = false;
    static String errorException = '';
    static String STR_INSERT = 'insert';
    static String STR_UPDATE = 'update';
    static String STR_DELETE = 'delete';
    
    public static void handlerBeforeInsert(List<Contact> contactsNew){
        conditionCreateContact(contactsNew,null,STR_INSERT);
        resizeContactPhoto(contactsNew,STR_INSERT);
    }

    public static void handlerBeforeUpdate(List<Contact> contactsNew){
        resizeContactPhoto(contactsNew,STR_UPDATE);
    }
    
    public static void conditionCreateContact(List<Contact> contactsNew,List<Contact> contactsOld,String eventMode){
        System.debug(':::: conditionCreateVisitReport Start ::::');
        
        List<Id> idAccountList = new List<Id>();
        List<Id> idUserList = new List<Id>();        
        
        for( Contact eachCont : contactsNew ){
            if( eachCont.AccountId != null ){
                idAccountList.add(eachCont.AccountId);
            }            
            idUserList.add(eachCont.OwnerId);
        }
        
        Map<Id,Account> queryAccount = new Map<Id,Account>([ select Id,OwnerId,Owner.Segment__c
                                                            from Account 
                                                            where Id IN :idAccountList ]);
        
        List<AccountTeamMember> queryAccountTeamMember = [ select accountId,userId
                                                          from AccountTeamMember 
                                                          where accountId IN :idAccountList 
                                                          and userId IN :idUserList];
        
        Map<Id,User> queryUser = new Map<Id,User>([select Id,UserRole.Name,Segment__c from user where Id IN :idUserList]);
		System.debug('queryAccount : '+queryAccount);
        System.debug('contactsNew : '+contactsNew);        
        for( Contact eachCont : contactsNew ){
            Boolean checkCreate = false;
            if( queryAccount.containsKey(eachCont.AccountId) ){                
                if( queryAccount.get(eachCont.AccountId).OwnerId == eachCont.OwnerId ){
                    checkCreate = true;
                }
                
                if(!checkCreate){
                    for(AccountTeamMember eachAccTeam : queryAccountTeamMember ){
                        if( eachCont.AccountId == eachAccTeam.AccountId && eachCont.OwnerId ==  eachAccTeam.UserId ){
                            checkCreate = true;
                            break;
                        }
                    }  
                }
            }
            
            if(!checkCreate){
                
                List<Account__c> privilegeList = Account__c.getall().values();
                
                for( Account__c eachPrivilege : privilegeList ){
                    if(queryUser.get(eachCont.OwnerId).UserRole.Name == eachPrivilege.Role__c 
                       && queryAccount.get(eachCont.AccountId).Owner.Segment__c == eachPrivilege.Segment__c){
                           checkCreate = true;
                       }                    
                }                            
            }

            if(!checkCreate){
                if(eachCont.Completed_Survey__c == true){
                    checkCreate = true;
                }  
            }
         
            if(!checkCreate){
                eachCont.addError( Trigger_Msg__c.getValues('Permission_Create_Opportunity').Description__c  ,false);    
            }
            System.debug('::::: checkCreate : '+checkCreate+' || '+eachCont.Name+' :::::');
        }   
        
        System.debug(':::: conditionCreateContact End ::::');
    }

    /*** To resize contact photo for generate Executive Summary PDF by Jittramas ***/
    public static void resizeContactPhoto(List<Contact> contactsNew,String eventMode){
        System.debug(':::: conditionCreateVisitReport Start ::::');
        String photo = null;
        String photoSrc = null;
        String photoStyle = null;
        for(Contact contactItem : contactsNew){
            Boolean checkCreate = false;
                System.debug('## contact photo original :'+ contactItem.Photo__c);
                if(contactItem.Photo__c != null){
                    checkCreate = true;
                    photo = contactItem.Photo__c;
                    photoSrc = photo.substringBetween('src="', '"');
                    if(photo.contains('style')){
                        photoStyle = photo.substringBetween('style=', '>');
                        photo = photo.replace(photoStyle, '"height: auto;width: 200px"');
                    }
                    else{
                        photoStyle = '"height: auto;width: 200px"';
                        photo = '<img alt="User-added image" src="'+photoSrc+'" style='+photoStyle+'></img>';
                    }

                    contactItem.Photo__c = photo;
                }else{
                    contactItem.Photo__c = null;
                    checkCreate = true;
                }
            if(!checkCreate){
                contactItem.addError('',false);
            }
        }
            
        System.debug(':::: conditionCreateVisitReport End ::::');
    }
    
    
}