@isTest
public class IDMSoapServiceTest {
/*
    static testmethod void SoapServiceTest() {
        	UserServiceMock servicemock = new UserServiceMock();
			UserService.setMockservice(servicemock);
        	
			try { IDMSoapService.InactiveUsersByROCodes(null); } catch(Exception e) { }
			try { IDMSoapService.CreateUsersByROCodes(null); } catch(Exception e) { }
			try { IDMSoapService.InquiryUsersByROCodes(null); } catch(Exception e) { }
            
            UserInfoDTO dto = new UserInfoDTO();
            dto.Alias='12346'; 
            dto.Email='stdus2@tmbbank.com';
            dto.EmailEncodingKey='UTF-8'; 
            dto.LastName='Testing2'; 
            dto.LanguageLocaleKey='en_US';
            dto.LocaleSidKey='en_US';
            dto.ProfileName = 'System Administrator';
            dto.UserRoleName ='SME Sales Management';
            dto.ROCode='12346';
            dto.IsActive='true';
            dto.TimeZoneSidKey='Asia/Bangkok';
            dto.UserName='12346@tmbbank.com';
            dto.DelegatedApproverId = String.Valueof([Select Id from User where IsActive=true Order by Name ASC  Limit 1 ].Id);
            dto.ManagerId = String.Valueof([Select Id from User where IsActive= true Order by Name DESC Limit 1 ].Id);
            dto.UserPreferencesHideS1BrowserUI='false';
            dto.UserPermissionsSupportUser='false';
        
            UserInfoDTO dto2 = new UserInfoDTO();
            dto2.Alias='12348'; 
            dto2.Email='stdus3@tmbbank.com';
            dto2.EmailEncodingKey='UTF-8'; 
            dto2.LastName='Testing3'; 
            dto2.LanguageLocaleKey='en_US';
            dto2.LocaleSidKey='en_US';
            dto2.ProfileName = 'System Administrator';
            dto2.UserRoleName ='SME Sales Management';
            dto2.ROCode='12348';
            dto2.IsActive='true';
            dto2.TimeZoneSidKey='Asia/Bangkok';
            dto2.UserName='12348@tmbbank.com';
            dto2.DelegatedApproverId = String.Valueof([Select Id from User where IsActive= true Order by Name ASC Limit 1 ].Id);
            dto2.ManagerId = String.Valueof([Select Id from User where IsActive= true Order by Name DESC Limit 1 ].Id);
            dto2.UserPreferencesHideS1BrowserUI='true';
            dto2.UserPermissionsSupportUser='true';
        
        
            List<UserInfoDTO> setRocodes = new List<UserInfoDTO> ();
            setRocodes.add(dto);
            setRocodes.add(dto2);
        
            List<String> setRocodes2 = new List<String> ();
            setRocodes2.add('12346');
            setRocodes2.add('12348');
			
			IDMSoapService.IDMRespond Response1 =IDMSoapService.InactiveUsersByROCodes(setRocodes2);
			IDMSoapService.IDMRespond Response2 =IDMSoapService.CreateUsersByROCodes(setRocodes);
            IDMSoapService.IDMUserRespond Response3 =IDMSoapService.InquiryUsersByROCodes(setRocodes2);

    }
    
    
    static testmethod void SoapServiceExceptionTest() {
			try { IDMSoapService.InactiveUsersByROCodes(null); } catch(Exception e) { }
			try { IDMSoapService.CreateUsersByROCodes(null); } catch(Exception e) { }
			try { IDMSoapService.InquiryUsersByROCodes(null); } catch(Exception e) { }
        
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        	User u2 = new User(Alias = '24477', Email='aasdd@tmbbank.com',UserRoleId = [select Id from UserRole where Name = : 'CEO' ].Id, 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',  
            LocaleSidKey='en_GB', ProfileId = p.Id,Employee_ID__c='24477',IsActive=true,
            TimeZoneSidKey='Asia/Bangkok', UserName='24477@tmbbank.com');
        	insert u2;
        
			UserInfoDTO dto = new UserInfoDTO();
            dto.Alias='12346'; 
            dto.Email='stdus2@tmbbank.com';
            dto.EmailEncodingKey='UTF-8'; 
            dto.LastName='Testing2'; 
            dto.LanguageLocaleKey='en_US';
            dto.LocaleSidKey='en_US';
            dto.ProfileName = 'System Administrator';
            dto.UserRoleName ='SME Sales Management';
            dto.ROCode='12346';
            dto.IsActive='true';
            dto.TimeZoneSidKey='Asia/Bangkok';
            dto.UserName='12346@tmbbank.com';
            dto.DelegatedApproverId = String.Valueof([Select Id from User where IsActive=true Order by Name ASC  Limit 1 ].Id);
        	dto.ManagerId = '24477';
        	system.debug('IDM dto.ManagerId: '+dto.ManagerId);
            dto.UserPreferencesHideS1BrowserUI='false';
            dto.UserPermissionsSupportUser='false';
        
            UserInfoDTO dto2 = new UserInfoDTO();
            dto2.Alias='12348'; 
            dto2.Email='stdus3@tmbbank.com';
            dto2.EmailEncodingKey='UTF-8'; 
            dto2.LastName='Testing3'; 
            dto2.LanguageLocaleKey='en_US';
            dto2.LocaleSidKey='en_US';
            dto2.ProfileName = 'System Administrator';
            dto2.UserRoleName ='SME Sales Management';
            dto2.ROCode='12348';
            dto2.IsActive='true';
            dto2.TimeZoneSidKey='Asia/Bangkok';
            dto2.UserName='12348@tmbbank.com';
            dto2.DelegatedApproverId = String.Valueof([Select Id from User where IsActive= true Order by Name ASC Limit 1 ].Id);
        	dto2.ManagerId = '24477';
        	system.debug('IDM dto2.ManagerId: '+dto2.ManagerId);
            dto2.UserPreferencesHideS1BrowserUI='true';
            dto2.UserPermissionsSupportUser='true';
        
        
            List<UserInfoDTO> setRocodes = new List<UserInfoDTO> ();
            setRocodes.add(dto);
            setRocodes.add(dto2);
        
            List<String> setRocodes2 = new List<String> ();
            setRocodes2.add('12346');
            setRocodes2.add('12348');
			
			IDMSoapService.IDMRespond Response1 =IDMSoapService.InactiveUsersByROCodes(setRocodes2);
			IDMSoapService.IDMRespond Response2 =IDMSoapService.CreateUsersByROCodes(setRocodes);
            IDMSoapService.IDMUserRespond Response3 =IDMSoapService.InquiryUsersByROCodes(setRocodes2);
    }
    
    
    public class UserServiceMock implements IUserService {
		public Boolean InactiveUsersByROCodes(Set<String> rocodes ) {
			return true;
		}
		public Boolean CreateUsersByROCodes(Set<UserInfoDTO> userinfo) {
			return true;
		}
		public List<UserInfoDTO> InquiryUsersByROCodes(Set<String> rocodes ) {
			List<UserInfoDTO> ret = new List<UserInfoDTO>();
            return ret;
		}
        
    }
    */
}