public abstract class AbstractCoreClass {
    public Object getProperty(String param_name) {
        Map<String, Object> untyped_instance = (Map<String, Object>)deserializeObject(this);
        return untyped_instance.get(param_name);
    }

    public static Boolean isNotEmpty(Object value) {
        return value != null && String.isNotEmpty(String.valueOf(value));
    }

    public static Object deserializeObject(Object obj) {
        return obj != null ? JSON.deserializeUntyped(JSON.serialize(obj)) : null;
    }

    public static Object getResult(String path, Object obj) {
        String key = path.substringBefore('.');
        if(obj == null) return null;
        if(path.contains('.')) {
            return Pattern.matches('([0-9]+)', key) ? getResult(path.substringAfter('.'), ((List<Object>)deserializeObject(obj)).get(Integer.valueOf(key))) : getResult(path.substringAfter('.'), ((Map<String, Object>)deserializeObject(obj)).get(key));
        }
        return Pattern.matches('([0-9]+)', key) ? ((List<Object>)deserializeObject(obj)).get(Integer.valueOf(key)) : ((Map<String, Object>)deserializeObject(obj)).get(key);
    }

    // v.1 one tier
    public static Object putObject(String key, Object objInput, Object obj) {
        Map<String, Object> tempObj = ((Map<String, Object>)deserializeObject(obj));
        tempObj.put(key, objInput);
        return tempObj;
    }

    // v.2 many tier only Key and value Map Class
    public static Object putObjectByPath(String path, Object objInput, Object obj) {
        String key = path.substringAfterLast('.');
        if(path.contains('.')) {
            Map<String, Object> tempObj = (Map<String, Object>)getResult(path.substringBeforeLast('.'), obj);
            if(tempObj.containsKey(key)) {
                tempObj.put(key, objInput);
                return putObjectByPath(path.substringBeforeLast('.'), tempObj, obj);
            }
            else {
                return putObjectByPath(path.substringBeforeLast('.'), new Map<String, Object>{ key => objInput }, obj);
            }
        }
        else {
            Map<String, Object> tempObj = (Map<String, Object>)obj;
            tempObj.put(path, objInput);
            return tempObj;
        }
    }
}