/* eslint-disable no-dupe-class-members */
/* eslint-disable no-loop-func */
/* eslint-disable handle-callback-err */
/* eslint-disable no-undef */
/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
import { LightningElement, track} from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import getMapGroup from '@salesforce/apex/fnaHighlightProductCtrl.getMapGroup';
import resourceQuestion from '@salesforce/resourceUrl/FNA_Resource';
import fnaJquery from '@salesforce/resourceUrl/fnaJquery';
import getThumbnailUrlAll from '@salesforce/apex/fnaHighlightProductCtrl.getThumbnailUrlAll';

import sendOTP from '@salesforce/apex/fnaHighlightProductCtrl.sendOTP'; 
import sendSMSURL from '@salesforce/apex/fnaUtility.sendSMSURL'; 
import successDownload from '@salesforce/apex/fnaUtility.successDownload';
import fnaCheckExisting from '@salesforce/apex/fnaUtility.FnaCheckExisting';

import getImage from '@salesforce/apex/fnaQuestionnaireTemplateCtrl.getImage'; 

import getAvatarDetail from '@salesforce/apex/fnaHighlightProductCtrl.getAvatarMasterDetail'; 
import getAvatar from '@salesforce/apex/fnaHighlightProductCtrl.getAvatar'; 

import fnGenerateOTP from '@salesforce/apex/fnaUtility.fnGenerateOTP';
import fnVerifyOTP from '@salesforce/apex/fnaUtility.fnVerifyOTP';

import idleTime from '@salesforce/apex/fnaHighlightProductCtrl.idleTime'; 
import createRef from '@salesforce/apex/fnaHighlightProductCtrl.createRef'; 

import callServiceOSC14 from '@salesforce/apexContinuation/fnaAvatarDetailCtrl.callServiceOSC14';
import callServiceOSC16 from '@salesforce/apexContinuation/fnaAvatarDetailCtrl.callServiceOSC16';
import stampOffSet from '@salesforce/apexContinuation/fnaAvatarDetailCtrl.stampOffSet';

// import decryptParams from '@salesforce/apex/fnaUtility.decryptParams';
import encryptParams from '@salesforce/apex/fnaUtility.encryptParams';

export default class fnaHighlightProduct extends LightningElement {

    @track getID = '';
    @track lstProductHighlight = [];
    @track lstProductNormal = [];
    @track isDownload = false;
    //@track taskAcction = 'btnScroll slds-button transition';
    //@track classDownloadAcction = '';
    //@track isMore = '';
    @track isHighlight = 'show';
    @track isNormal = 'hide';
    @track isBtn = 'show';
    @track isLine = 'hide';
   
    @track imageResource = '';
    //@track imageResourceProduct = [];
    @track bgDisplay = 'bg hide';
    @track loadingDisplay = 'hideLoading';

    @track titleBtn = 'ผลิตภัณฑ์อื่นๆ ที่เหมาะกับคุณ';
    @track icoBtnLess = 'hide';
    @track icoBtnMore = 'show';

    @track headerTitle = 'ผลิตภัณฑ์ที่ตอบโจทย์ความต้องการของ';
    @track b64 = '';
    //@track name1 = '';
    //@track sex = '';
    // @track tag = '';
    //@track imageThumbnail;

    @track phoneNumber = '';
    phone = '';
    scrollInitialized = false;
    renderedCallback() {
        if (this.scrollInitialized) {
            return;
        }

        this.scrollInitialized = true;
        Promise.all([
            loadScript(this, fnaJquery),
        ])
        .then(() => {
            // console.log('Success load jquery script');
            Promise.all([
                loadScript(this, resourceQuestion + '/public/js/bootstrap.min.js'),
                loadScript(this, resourceQuestion + '/public/js/bootstrap-slider.js'),
                loadScript(this, resourceQuestion + '/public/js/jquery.jInvertScroll.js'),
                loadScript(this, resourceQuestion + '/public/js/jquery-ui.js'),
                loadScript(this, resourceQuestion + '/src/js/script.js'),
                
                loadStyle(this, resourceQuestion + '/public/css/bootstrap.min.css'),
                loadStyle(this, resourceQuestion + '/public/css/hover.css'),
                loadStyle(this, resourceQuestion + '/src/css/styles.css'),
                // loadStyle(this, 'https://fonts.googleapis.com/icon?family=Material+Icons')
                
            ])
            .then(() => {
                // console.log('Success load all script and CSS');
                this.initializeScript();
            })
            .catch(error => {
                console.log('Failed load script : ' + error);
            });
        })
        .catch(error => {
            console.log('Failed load script : ' + error);
        });
            
    }

    constructor(){
        super();
        this.loadingDisplay = 'showLoading';

        this.getImageResource();
        const sPageURL = decodeURIComponent(window.location.search.substring(1));
        const parameter = this.getparameter(sPageURL);
        
        // sessionStorage.setItem("lastname", "Smith");
        // console.log('session ' + sessionStorage.getItem("lastname"));

        this.sessionPage = sessionStorage.getItem("1");
        // console.log('sessionData ' + this.sessionPage);

        // this.getSession(sessionData);
        
        // console.log(parameter);
        // console.log(parameter.has('Id'));
        if(parameter.has('Id')){
            
            this.getID = parameter.get('Id');
            this.mapGroup(this.getID);
            this.getProductDetail(this.getID, window.location.search);
            this.getAvatarName(this.getID);

        }else{
            // console.log('parametersdfsdf');
        }
        // console.log('sessionStorage ' + sessionStorage.getItem("1"));

        //this.classDownloadAcction = this.taskAcction;
        // this.loadingDisplay = 'hideLoading';
    }

    getImageResource(){

        getImage()
        .then(result => {
            this.imageResource = result;
            this.imgExit = result.cancel;
            this.imgExitInvert = result.cancelInvert;
            this.bgDisplay = 'bg show';
            // console.log('succes load Image');  
        })
        .catch(error => {
            console.log('catch get Image');
            console.log(error);
        });
    }

    @track avatar = {};
    getAvatarName(formId){
        getAvatar({idForm: formId})
        .then(result => {
            // console.log('getAvatar :',result)
            this.avatar = result;
        })
        .catch(error => { 
            console.log('catch');
            console.log(error);
        });
    }

    getProductDetail(formId, sUrlPage){
        // console.log('Form ID ' + formId);
        // if(!this.isDownload){
            getAvatarDetail({idForm: formId, urlDropOff: sUrlPage})
            .then(result => {
                // console.log('getProductDetail :',result)
                this.b64 = result.avatarImage;
                this.phoneNumber = result.mobileNumber;
                this.phone = result.mobileNumber;
                // console.log('then ' + result);
            })
            .catch(error => { 
                console.log('catch');
                console.log(error);
            });
        // }
    }

    url = '';
    //PRE-LOADING IMAGES SECTION
    preLoadImagesH(event){
        let targetThumbnailHId = event.target.dataset.srcIdThumbnailHighlight;

        let targetH = this.template.querySelector(`[data-src-id-preload-highlight="${targetThumbnailHId}"]`);

        targetH.classList.add("img-fluid","hideLoading");

        event.target.classList.remove("hideLoading");

        // console.log('Loading Highlight Product')
    }

    preLoadImagesN(event){
        let targetThumbnailNId = event.target.dataset.srcIdThumbnailNormal;

        let targetN = this.template.querySelector(`[data-src-id-preload-normal="${targetThumbnailNId}"]`);

        targetN.classList.add("img-fluid","hideLoading");

        event.target.classList.remove("hideLoading");

        // console.log('Loading Normal Product')
    }
    //PRE-LOADING IMAGES SECTION

    @track timeoutFn;
    
    mapGroup(rec) {

        getMapGroup({fnaActivityId: rec})
        .then(result => {
            // var count = 0;
            
            // console.log('Normal Product: ',result.resProdNormal)
            // console.log('Highlight Product',result.resProdHightlight)
    

            this.resProdAll = result.resProdNormal;
            this.isDownload = result.resIsDownload;
            // var statusNow = this.status;
            // var statusNowUpdate;
         
           
            if(result.resProdHightlight.length === 0 && result.resProdNormal.length !== 0){
                // console.log('-----Highlight Product Empty------')
                // statusNowUpdate = 'Normal';
                this.isNormal = 'show';
                this.isHighlight = 'hide';
                this.isBtn = 'hide';
                this.isLine = 'hide';
                // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdNormal)),count,statusNowUpdate);
                // console.log('====================')
                getThumbnailUrlAll({prodFNA: result.resProdNormal})
                .then(resp => {
                    // console.log('lstProductNormal : ',resp)
                    let tempNormal = [];
                    this.lstProductNormal = tempNormal.concat(resp)
                    this.loadingDisplay = 'hideLoading';
                    // this.bShowModal = true;
                    this.bShowModal1 = false;
                    this.bShowModal3 = true;
                    this.bShowModal2 = false;
                    // START SESSION TIMEOUT
                    var sessionTempHighlight = sessionStorage.getItem("1");
                    // console.log('sessionTempHighlight : ',sessionTempHighlight);
                    if(sessionTempHighlight !== null){
                        this.timeoutFn = idleTime()
                        .then(idleTimeMillisec => {
                            // console.log('idleTimeMillisec : ',idleTimeMillisec)
                            let idleTimeTemp = parseInt(idleTimeMillisec,10);
                            // console.log('idle time to timeout : ', idleTimeTemp);
                            setTimeout(() => {  
                                // console.log('timeout')
                                // console.log('sessionTempHighlight :', sessionTempHighlight)
                                // console.log('getID : ',this.getID)
                                createRef({fnaActId: this.getID,sessionData: sessionTempHighlight})
                                .then(resultref => {
                                    // console.log('create ref complete')
                                    // console.log(resultref)
                                    sessionStorage.clear();
                                    this.loadingDisplay = 'showLoading';
                                    this.url += 'avatardetail?Id=' + this.getID;
                                    this.gotoURL(this.url);
                                })
                                .catch(er => {
                                    console.log('create ref catch');
                                    console.log(er)
                                })
                            }, idleTimeMillisec);
                            // timeTrackFn = timeFn;
                        })
                        .catch(err => {
                            console.log('idleTime catch');
                            console.log(err)
                        })
                    }
                    // END SESSION TIMEOUT

                })
                .catch(errr =>{
                    console.log(errr)
                })
                // console.log('====================')
            }
            else if (result.resProdNormal.length === 0 && result.resProdHightlight.length !== 0){
                // console.log('-----Normal Product Empty------')
                // statusNowUpdate = 'Highlight';
                this.isNormal = 'hide';
                this.isHighlight = 'show';
                this.isBtn = 'hide';
                // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdHightlight)),count,statusNowUpdate);
                // console.log('====================')
                getThumbnailUrlAll({prodFNA: result.resProdHightlight})
                .then(resp => {
                    // console.log('lstProductHighlight : ',resp)
                    let tempHighlight = [];
                    this.lstProductHighlight = tempHighlight.concat(resp)
                    this.loadingDisplay = 'hideLoading';
                    this.bShowModal1 = false;
                    this.bShowModal3 = true;
                    this.bShowModal2 = false;

                    // START SESSION TIMEOUT
                    var sessionTempHighlight = sessionStorage.getItem("1");
                    // console.log('sessionTempHighlight : ',sessionTempHighlight);
                    if(sessionTempHighlight !== null){
                        this.timeoutFn = idleTime()
                        .then(idleTimeMillisec => {
                            // console.log('idleTimeMillisec : ',idleTimeMillisec)
                            let idleTimeTemp = parseInt(idleTimeMillisec,10);
                            // console.log('idle time to timeout : ', idleTimeTemp);
                            setTimeout(() => {  
                                // console.log('timeout')
                                // console.log('sessionTempHighlight :', sessionTempHighlight)
                                // console.log('getID : ',this.getID)
                                createRef({fnaActId: this.getID,sessionData: sessionTempHighlight})
                                .then(resultref => {
                                    // console.log('create ref complete')
                                    // console.log(resultref)
                                    sessionStorage.clear();
                                    this.loadingDisplay = 'showLoading';
                                    this.url += 'avatardetail?Id=' + this.getID;
                                    this.gotoURL(this.url);
                                })
                                .catch(er => {
                                    console.log('create ref catch');
                                    console.log(er)
                                })
                            }, idleTimeMillisec);
                            // timeTrackFn = timeFn;
                        })
                        .catch(err => {
                            console.log('idleTime catch');
                            console.log(err)
                        })
                    }
                    // END SESSION TIMEOUT
                    
                })
                .catch(errr =>{
                    console.log(errr)
                })
                // console.log('====================')
            }
          
            else if (result.resProdNormal.length === 0 && result.resProdHightlight.length === 0){
                // console.log('-----Normal Product Empty------')
                // console.log('-----Highlight Product Empty------')
                this.isNormal = 'hide';
                this.isHighlight = 'hide';
                this.isBtn = 'hide';
                this.headerTitle = '404 Product Not Found'
                this.loadingDisplay = 'hideLoading';

                this.bShowModal1 = false;
                this.bShowModal3 = true;
                this.bShowModal2 = false;
            }
            else{
                
              
                // console.log('-----Product not empty------')
                // statusNowUpdate = 'Highlight';
                this.isLine = 'show';
                // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdHightlight)),count,statusNowUpdate);
                // console.log('====================')
                getThumbnailUrlAll({prodFNA: result.resProdHightlight})
                .then(resp => {
                    // console.log('lstProductHighlight : ',resp)
                    let tempHighlight = [];
                    this.lstProductHighlight = tempHighlight.concat(resp)
                    this.loadingDisplay = 'hideLoading';

                    this.bShowModal1 = false;
                    this.bShowModal3 = true;
                    this.bShowModal2 = false;

                    // START SESSION TIMEOUT
                    var sessionTempHighlight = sessionStorage.getItem("1");
                    // console.log('sessionTempHighlight : ',sessionTempHighlight);
                    if(sessionTempHighlight !== null){
                        this.timeoutFn = idleTime()
                        .then(idleTimeMillisec => {
                            // console.log('idleTimeMillisec : ',idleTimeMillisec)
                            let idleTimeTemp = parseInt(idleTimeMillisec,10);
                            // console.log('idle time to timeout : ', idleTimeTemp);
                            setTimeout(() => {  
                                // console.log('timeout')
                                // console.log('sessionTempHighlight :', sessionTempHighlight)
                                // console.log('getID : ',this.getID)
                                createRef({fnaActId: this.getID,sessionData: sessionTempHighlight})
                                .then(resultref => {
                                    // console.log('create ref complete')
                                    // console.log(resultref)
                                    sessionStorage.clear();
                                    this.loadingDisplay = 'showLoading';
                                    this.url += 'avatardetail?Id=' + this.getID;
                                    this.gotoURL(this.url);
                                })
                                .catch(er => {
                                    console.log('create ref catch');
                                    console.log(er)
                                })
                            }, idleTimeMillisec);
                            // timeTrackFn = timeFn;
                        })
                        .catch(err => {
                            console.log('idleTime catch');
                            console.log(err)
                        })
                    }
                    // END SESSION TIMEOUT
                    
                })
                .catch(errr =>{
                    console.log(errr)
                })
                // console.log('====================')
            }
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }


    @track statusNow = 'Highlight';
    @track resProdAll = [];
    // mapGroup(rec) {
    //     getMapGroup({fnaActivityId: rec})
    //     .then(result => {
    //         var count = 0;
    //         console.log('Normal Product: ',result.resProdNormal)
    //         console.log('Highlight Product',result.resProdHightlight)
    //         this.resProdAll = result.resProdNormal;
    //         this.isDownload = result.resIsDownload;
    //         // var statusNow = this.status;
    //         // var statusNowUpdate;
    //         if(result.resProdHightlight.length === 0 && result.resProdNormal.length !== 0){
    //             console.log('-----Highlight Product Empty------')
    //             statusNowUpdate = 'Normal';
    //             this.isNormal = 'show';
    //             this.isHighlight = 'hide';
    //             this.isBtn = 'hide';
    //             // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdNormal)),count,statusNowUpdate);
    //             console.log('====================')
    //             getThumbnailUrlAll({prodFNA: result.resProdNormal})
    //             .then(resp => {
    //                 console.log(resp)
    //             })
    //             .catch(errr =>{
    //                 console.log(errr)
    //             })
    //             console.log('====================')
    //         }
    //         else if (result.resProdNormal.length === 0 && result.resProdHightlight.length !== 0){
    //             console.log('-----Normal Product Empty------')
    //             statusNowUpdate = 'Highlight';
    //             this.isNormal = 'hide';
    //             this.isHighlight = 'show';
    //             this.isBtn = 'hide';
    //             // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdHightlight)),count,statusNowUpdate);
    //         }
    //         else if (result.resProdNormal.length === 0 && result.resProdHightlight.length === 0){
    //             console.log('-----Normal Product Empty------')
    //             console.log('-----Highlight Product Empty------')
    //             this.isNormal = 'hide';
    //             this.isHighlight = 'hide';
    //             this.isBtn = 'hide';
    //             this.headerTitle = '404 Product Not Found'
    //             this.loadingDisplay = 'hideLoading';
    //         }
    //         else{
    //             console.log('-----Product not empty------')
    //             statusNowUpdate = 'Highlight';
    //             this.isLine = 'show';
    //             // this.getThumbnail(JSON.parse(JSON.stringify(result.resProdHightlight)),count,statusNowUpdate);
    //             console.log('====================')
    //             getThumbnailUrlAll({prodFNA: result.resProdHightlight})
    //             .then(resp => {
    //                 console.log(resp)
    //             })
    //             .catch(errr =>{
    //                 console.log(errr)
    //             })
    //             console.log('====================')
    //         }
    //     })
    //     .catch(error => {
    //         console.log('catch');
    //         console.log(error);
    //     });
    // }
    
    // }





    @track checkBool = false;
    
    moreProduct(event) {
       
        if(this.isNormal === 'hide') {
            this.isNormal = '';
            this.titleBtn = 'ผลิตภัณฑ์ที่ตอบโจทย์ความต้องการ';
            let iBtnL = this.template.querySelector(`[data-id="iconBtnL"]`);
            let iBtnR = this.template.querySelector(`[data-id="iconBtnR"]`);
            iBtnL.src = this.imageResource.less;
            iBtnR.src = this.imageResource.less;
        } else {
            this.isNormal = 'hide';
            this.titleBtn = 'ผลิตภัณฑ์อื่นๆ ที่เหมาะกับคุณ';
            let iBtnL = this.template.querySelector(`[data-id="iconBtnL"]`);
            let iBtnR = this.template.querySelector(`[data-id="iconBtnR"]`);
            iBtnL.src = this.imageResource.more;
            iBtnR.src = this.imageResource.more;
            // let targetId = event.target.dataset.targetId;
            // let target = this.template.querySelector(`[data-id="${targetId}"]`);
            let target = this.template.querySelector(`[data-id="highlight-products"]`);
            target.scrollIntoView();
            // console.log('target id '+ targetId)
            // console.log('ttarget :',target)
        }
        if(this.resProdAll !== null){
            if(this.checkBool === false){
                this.loadingDisplay = 'showLoading';
                // var count = 0;
                // this.statusNow = 'Normal';
                // var statusNowUpdate = 'Normal';
                // console.log('this.resProdAll : ',this.resProdAll)
                // console.log('====================')
                getThumbnailUrlAll({prodFNA: this.resProdAll})
                .then(resp => {
                    // console.log('lstProductNormal : ',resp)
                    let tempNormal = [];
                    this.lstProductNormal = tempNormal.concat(resp)
                    this.loadingDisplay = 'hideLoading';
                    
                    // START SESSION TIMEOUT
                    var sessionTempHighlight = sessionStorage.getItem("1");
                    // console.log('sessionTempHighlight : ',sessionTempHighlight);
                    if(sessionTempHighlight !== null){
                        clearTimeout(this.timeoutFn);
                        this.timeoutFn = idleTime()
                        .then(idleTimeMillisec => {
                            // console.log('idleTimeMillisec : ',idleTimeMillisec)
                            let idleTimeTemp = parseInt(idleTimeMillisec,10);
                            // console.log('idle time to timeout : ', idleTimeTemp);
                            setTimeout(() => {  
                                // console.log('timeout')
                                // console.log('sessionTempHighlight :', sessionTempHighlight)
                                // console.log('getID : ',this.getID)
                                createRef({fnaActId: this.getID,sessionData: sessionTempHighlight})
                                .then(resultref => {
                                    // console.log('create ref complete')
                                    // console.log(resultref)
                                    sessionStorage.clear();
                                    this.loadingDisplay = 'showLoading';
                                    this.url += 'avatardetail?Id=' + this.getID;
                                    this.gotoURL(this.url);
                                })
                                .catch(er => {
                                    console.log('create ref catch');
                                    console.log(er)
                                })
                            }, idleTimeMillisec);
                            // timeTrackFn = timeFn;
                        })
                        .catch(err => {
                            console.log('idleTime catch');
                            console.log(err)
                        })
                    }
                    // END SESSION TIMEOUT
                  
                })
                .catch(errr =>{
                    console.log(errr)
                })
                // console.log('====================')
                // this.getThumbnail(JSON.parse(JSON.stringify(this.resProdAll)),count,statusNowUpdate);
                // this.getThumbnail(this.resProdAll,count,statusNow);
                this.checkBool = true;
            }
        }
    }
    // moreProduct(event) {
    //     if(this.isNormal === 'hide') {
    //         this.isNormal = '';
    //         this.titleBtn = 'ดูผลิตภัณฑ์แนะนำ';
    //         this.scrollFix = 'scrollHere';
    //     } else {
    //         this.isNormal = 'hide';
    //         this.titleBtn = 'ดูผลิตภัณฑ์ทั้งหมด';
            
    //         let targetId = event.target.dataset.targetId;
    //         let target = this.template.querySelector(`[data-id="${targetId}"]`);
    //         target.scrollIntoView();
    //         console.log('target id '+ targetId)
    //         console.log('ttarget :',target)
    //     }
    //     if(this.resProdAll !== null){
    //         if(this.checkBool === false){
    //             this.loadingDisplay = 'showLoading';
    //             var count = 0;
    //             // this.statusNow = 'Normal';
    //             var statusNowUpdate = 'Normal';
    //             console.log('this.resProdAll : ',this.resProdAll)
    //             this.getThumbnail(JSON.parse(JSON.stringify(this.resProdAll)),count,statusNowUpdate);
    //             // this.getThumbnail(this.resProdAll,count,statusNow);
    //             this.checkBool = true;
    //         }
    //     }
    // }

    @track sessionPage;
    // @track session = sessionStorage.getItem("1");

    // getSession(session){
    //     console.log('session : ',session)
    // }

    // getThumbnail(resProd,cout,stat){
    //     var lengthProd = resProd.length;
    //     console.log('lengthProd',lengthProd)
    //     // console.log('session : ', this.session)

    //     getThumbnailUrl({prodFNA: resProd[cout]})
    //     .then(response => {
    //         console.log('then :',response)
    //         console.log('in',cout)
    //         if(stat === 'Highlight'){
    //             this.lstProductHighlight.push(response);
    //             if(cout === lengthProd-1){
    //                 this.loadingDisplay = 'hideLoading';

    //                 // START SESSION TIMEOUT
    //                 var sessionTempHighlight = sessionStorage.getItem("1");
    //                 // console.log('sessionTempHighlight : ',sessionTempHighlight);
    //                 if(sessionTempHighlight !== null){
    //                     idleTime()
    //                     .then(idleTimeMillisec => {
    //                         // console.log('idleTimeMillisec : ',idleTimeMillisec)
    //                         let idleTimeTemp = parseInt(idleTimeMillisec,10);
    //                         console.log('idle time to timeout : ', idleTimeTemp);
    //                         setTimeout(() => {  
    //                             console.log('timeout')
    //                             console.log('sessionTempHighlight :', sessionTempHighlight)
    //                             console.log('getID : ',this.getID)
    //                             createRef({fnaActId: this.getID,sessionData: sessionTempHighlight})
    //                             .then(result => {
    //                                 console.log('create ref complete')
    //                                 console.log(result)
    //                             })
    //                             .catch(er => {
    //                                 console.log('create ref catch');
    //                                 console.log(er)
    //                             })
    //                         }, idleTimeMillisec);
    //                         // timeTrackFn = timeFn;
    //                     })
    //                     .catch(err => {
    //                         console.log('idleTime catch');
    //                         console.log(err)
    //                     })
    //                 }
    //                 // END SESSION TIMEOUT
    //             }
    //         }
    //         else if (stat === 'Normal'){
    //             this.lstProductNormal.push(response);
    //             if(cout === lengthProd-1){
    //                 this.loadingDisplay = 'hideLoading';
    //                 var sessionTempNormal = sessionStorage.getItem("1");
    //                 // console.log('sessionTempNormal : ',sessionTempNormal);
    //                 if(sessionTempNormal !== null){
    //                     idleTime()
    //                     .then(idleTimeMillisec => {
    //                         // console.log('idleTimeMillisec : ',idleTimeMillisec)
    //                         let idleTimeTemp = parseInt(idleTimeMillisec,10);
    //                         console.log('idle time to timeout : ', idleTimeTemp);
    //                         setTimeout(() => {
    //                             console.log('timeout')
    //                             console.log('sessionTempNormal :', sessionTempNormal)
    //                             console.log('getID : ',this.getID)
    //                             createRef({fnaActId: this.getID,sessionData: sessionTempNormal})
    //                             .then(result => {
    //                                 console.log('create ref complete')
    //                                 console.log(result)
    //                             })
    //                             .catch(er => {
    //                                 console.log('create ref catch');
    //                                 console.log(er)
    //                             })
    //                         }, idleTimeMillisec);
    //                         // timeTrackFn = timeFn;
    //                     })
    //                     .catch(err => {
    //                         console.log('idleTime catch');
    //                         console.log(err)
    //                     })
    //                 }
    //             }
    //         }
    //         cout++;
    //         if(cout < lengthProd){
    //             this.getThumbnail(resProd,cout,stat);
    //         }
    //     })
    //     .catch(err => {
    //         console.log('catch');
    //         console.log(err)
    //     })
    // }

    clearStorage(){
        localStorage.removeItem("Id");
        localStorage.removeItem("pId");
        localStorage.removeItem("qId");
    }

    exitPage(){
        this.loadingDisplay = 'showLoading';
        this.clearStorage();
        var urlHome = "/fin/s/";
        this.gotoURL(urlHome);
    }

    gotoURL(urlpage){
        //window.location.assign(urlpage);
        window.location.href = urlpage;
    }   

    gotoProductDetail(event) {
        // var productId = this.getID;
        this.loadingDisplay = 'showLoading';
        var RTLId = event.currentTarget.dataset.productid;

        // var originalURL = 'Id='+this.getID+'&RTLId='+RTLId;
        // // var encryptUrl = '/fin/fnaProductDetail?' + encodeURIComponent(originalURL);
        // var encryptUrl = '/fin/fnaProductDetail?' + encodeURIComponent(originalURL);
        // console.log('encryptUrl :',encryptUrl);
        // this.gotoURL(encryptUrl);

        encryptParams({FNAId: RTLId})
        .then(response => {
            // console.log('then',response);
            var productId = response;
            var originalURL = 'Id='+this.getID+'&RTLId='+productId;
            var encryptUrl = '/fin/fnaProductDetail?' + encodeURIComponent(originalURL);
            // console.log('encryptUrl :',encryptUrl);
            this.gotoURL(encryptUrl);

        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });



    }

    getparameter(param){
        var myMap = new Map();
        if(param !== ''){
            var sURLVariables = param.split('&');
        
            var i;
            var sParameterName;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                myMap.set(sParameterName[0], sParameterName[1]);
            }
        }
        // console.log('myMap : ', myMap)
        return myMap;
    }

    @track bShowModal  = false;
    @track bShowModal1 = false;
    @track bShowModal2  = false;
    @track bShowModal3  = false;
    @track err = 'errorlog hideLoading';
    @track textErr = '';
    @track textRef = '';
    @track btnResetOTP = 'showBtn';

    @track errPhone = 'errorlog hideLoading';

    closeModal() {    
        if(this.phoneNumber === ''){
            this.phoneNumber = this.phone;
        }
        
        this.bShowModal = false;
        this.bShowModal1 = false;
        this.bShowModal2 = false;
        this.bShowModal3 = false;
        this.btnResetOTP = 'showBtn';
        this.errPhone = 'errorlog hideLoading';
    }

    handleClick() {
        this.bShowModal = true;
        this.bShowModal1 = true;
        this.bShowModal2 = false;
        this.bShowModal3 = false;
        this.err = 'errorlog hideLoading';
        this.textErr = ''; 
    }
    
    // next(){
    //     fnGenerateOTP({fnaId: this.getID})
    //     .then(response => {
    //         console.log('then',response);
    //         this.textRef = response.Otp_Ref__c;
    //         this.bShowModal = true;
    //         this.bShowModal1 = false;
    //         this.bShowModal2 = true;
    //         this.bShowModal3 = false;
    //         this.btnResetOTP = 'hideBtn';
    //     })
    //     .catch(error => {
    //         console.log('catch');
    //         console.log(error);
    //     });
    // }

    allowNumbersOnly(e) {
        // console.log('allowNumbersOnly')
        let inputPhone = this.template.querySelector(`[data-id="phoneInput"]`);
        var code = (e.which) ? e.which : e.keyCode;
        if (code > 31 && (code < 48 || code > 57)) {
            // console.log('prevent')
            e.preventDefault();
        }
        inputPhone.classList.remove('inputInvalid');
        this.errPhone = 'errorlog hideLoading';
    }

    next(){
        let inputPhone = this.template.querySelector(`[data-id="phoneInput"]`);

        // console.log((inputPhone.value).length)
        var phoneno = /^[0]{1}[6,8,9]{1}[0-9]{8}$/;
        if(inputPhone.value.match(phoneno) && (inputPhone.value).length === 10) {
            // console.log('true')
            fnGenerateOTP({fnaId: this.getID})
            .then(response => {
                // console.log('then',response);
                this.textRef = response.Otp_Ref__c;
                this.bShowModal = true;
                this.bShowModal1 = false;
                this.bShowModal2 = true;
                this.bShowModal3 = false;
                this.btnResetOTP = 'showBtn';
                this.errPhone = 'errorlog hideLoading';
                inputPhone.classList.remove('inputInvalid');

                this.phoneNumber = inputPhone.value;
                this.phone = inputPhone.value;
                this.sendSMSOTP(this.phoneNumber, response.Id);
            })
            .catch(error => {
                console.log('catch');
                console.log(error);
            });
        }
        else {
            this.phoneNumber = '';
            this.errPhone = 'errorlog showLoading';
            // console.log('false')
            inputPhone.classList.add('inputInvalid');
        }
    }

    checkNext(event){
        // console.log('checkNext = ',event.target.value)
        var inputName = event.target.name;
        // console.log('inputName = ',inputName)

        let inputOTP_1 = this.template.querySelector(`[data-id="otpInput_1"]`);
        let inputOTP_2 = this.template.querySelector(`[data-id="otpInput_2"]`);
        let inputOTP_3 = this.template.querySelector(`[data-id="otpInput_3"]`);
        let inputOTP_4 = this.template.querySelector(`[data-id="otpInput_4"]`);
        let inputOTP_5 = this.template.querySelector(`[data-id="otpInput_5"]`);
        let inputOTP_6 = this.template.querySelector(`[data-id="otpInput_6"]`);
        let btnSubmitOTP = this.template.querySelector(`[data-id="btnSubmitOTP"]`);

        if(inputOTP_1.value === '' || 
        inputOTP_2.value === '' || 
        inputOTP_3.value === '' ||
        inputOTP_4.value === '' ||
        inputOTP_5.value === '' ||
        inputOTP_6.value === ''){
            this.textErr = '';
        }
        
        // (this.template.querySelector(`[data-id="otpInput_6"]`)).value || 
        if(inputName === 'submitOTP'){
            if( (inputOTP_1).value === '' || 
            (inputOTP_2).value === '' ||
            (inputOTP_3).value === '' ||
            (inputOTP_4).value === '' ||
            (inputOTP_5).value === '' ||
            (inputOTP_6).value === '' ){
                // console.log('OTP กรอกไม่ครบ')
                this.err = 'errorlog showLoading';
                // this.textErr = 'Please fill OTP Code.'; 
                this.textErr = 'กรุณาระบุรหัส OTP ที่ได้รับ';  
                this.btnResetOTP = 'showBtn';
            }
            else{
                // console.log('OTP กรอกครบกำลัง validate')
                let inputValue_1 = (inputOTP_1.value).toString();
                let inputValue_2 = (inputOTP_2.value).toString();
                let inputValue_3 = (inputOTP_3.value).toString();
                let inputValue_4 = (inputOTP_4.value).toString();
                let inputValue_5 = (inputOTP_5.value).toString();
                let inputValue_6 = (inputOTP_6.value).toString();
                var inputValue = inputValue_1 + inputValue_2 + inputValue_3 + inputValue_4 + inputValue_5 + inputValue_6;
                // console.log('inputValue : ',inputValue)
                fnVerifyOTP({fnaId: this.getID,otpCode: inputValue})
                .then(response => {
                    // console.log('then', response)
                    this.err = 'errorlog showLoading';
                    switch(response){
                        case 1:
                            // this.textErr = 'Please request the new OTP.';
                            this.textErr = 'ขอรับรหัส OTP ใหม่';
                            inputOTP_1.setAttribute("disabled", "");
                            inputOTP_2.setAttribute("disabled", "");
                            inputOTP_3.setAttribute("disabled", "");
                            inputOTP_4.setAttribute("disabled", "");
                            inputOTP_5.setAttribute("disabled", "");
                            inputOTP_6.setAttribute("disabled", "");
                            btnSubmitOTP.setAttribute("disabled", "");
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 2:
                            // this.textErr = 'Cannot use old OTP Code.';
                            this.textErr = 'รหัส OTP ไม่ถูกต้อง คุณอาจนำรหัสที่ถูกใช้งานไปแล้วมากรอก กรุณาตรวจสอบ SMS ใหม่ หรือ ขอรหัส OTP ใหม่';
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 3://disabled
                            // this.textErr = 'OTP Code was expired. Please request the new OTP.';
                            this.textErr = 'รหัส OTP ของคุณหมดอายุ กรุณาขอรับรหัส OTP ใหม่';
                            inputOTP_1.setAttribute("disabled", "");
                            inputOTP_2.setAttribute("disabled", "");
                            inputOTP_3.setAttribute("disabled", "");
                            inputOTP_4.setAttribute("disabled", "");
                            inputOTP_5.setAttribute("disabled", "");
                            inputOTP_6.setAttribute("disabled", "");
                            btnSubmitOTP.setAttribute("disabled", "");
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 4://disabled
                            // this.textErr = 'OTP Code was hit max retry. Please request the new OTP.';
                            this.textErr = 'ขออภัยค่ะ คุณกรอกรหัสผิดเกินจำนวนครั้งที่กำหนด กรุณาขอรับรหัส OTP ใหม่';
                            inputOTP_1.setAttribute("disabled", "");
                            inputOTP_2.setAttribute("disabled", "");
                            inputOTP_3.setAttribute("disabled", "");
                            inputOTP_4.setAttribute("disabled", "");
                            inputOTP_5.setAttribute("disabled", "");
                            inputOTP_6.setAttribute("disabled", "");
                            btnSubmitOTP.setAttribute("disabled", "");
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 5:
                            // this.textErr = 'OTP Code miss match.';
                            this.textErr = 'รหัส OTP ไม่ถูกต้อง กรุณาตรวจสอบ SMS ใหม่ หรือ ขอรับรหัส OTP ใหม่';
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 6://disabled
                            // this.textErr = 'Cannot use old OTP Code. Please request the new OTP.';
                            this.textErr = 'รหัส OTP ไม่ถูกต้อง คุณอาจนำรหัสที่ถูกใช้งานไปแล้วมากรอก กรุณาตรวจสอบ SMS ใหม่ หรือ ขอรหัส OTP ใหม่';
                            inputOTP_1.setAttribute("disabled", "");
                            inputOTP_2.setAttribute("disabled", "");
                            inputOTP_3.setAttribute("disabled", "");
                            inputOTP_4.setAttribute("disabled", "");
                            inputOTP_5.setAttribute("disabled", "");
                            inputOTP_6.setAttribute("disabled", "");
                            btnSubmitOTP.setAttribute("disabled", "");
                            this.btnResetOTP = 'showBtn';
                            break;
                        case 7:
                            this.err = 'successlog showLoading';
                            this.textErr = 'Verify success.';
                            inputOTP_1.setAttribute("disabled", "");
                            inputOTP_2.setAttribute("disabled", "");
                            inputOTP_3.setAttribute("disabled", "");
                            inputOTP_4.setAttribute("disabled", "");
                            inputOTP_5.setAttribute("disabled", "");
                            inputOTP_6.setAttribute("disabled", "");
                            if(inputName === 'submitOTP'){
                                this.checkExsiting();
                                this.loadingDisplay = 'showLoading';
                                this.bShowModal = true;
                                // this.bShowModal1 = false;
                                // this.bShowModal3 = true;
                                // this.bShowModal2 = false;
                                this.err = 'errorlog hideLoading';
                            }
                            break;
                        default:
                            break;
                    }
                })
                .catch(error => { 
                    console.log('catch');
                    console.log(error);
                });
            }
        }
        else{
            // console.log('value in OTPInput 6 =  null')
        }
        // console.log('err : ', this.err)

      
        if(event.target.value !== ''){
            var inputNameCount = inputName.substring(inputName.length-1, inputName.length);
            var integer = parseInt(inputNameCount, 10);
            integer = integer+1;
            var otpInput;

            switch (true) {
                case (integer === 1 && event.target.value !== ''):
                    otpInput = inputOTP_1;
                    // console.log(event.target.value);  
                    break;
                case (integer === 2 && event.target.value !== ''):
                    otpInput = inputOTP_2;
                    // console.log(event.target.value);        
                    break;
                case (integer === 3 && event.target.value !== ''):
                    otpInput = inputOTP_3;
                    // console.log(event.target.value);             
                    break;
                case (integer === 4 && event.target.value !== ''):
                    otpInput = inputOTP_4;      
                    // console.log(event.target.value);        
                    break;
                case (integer === 5 && event.target.value !== ''):
                    otpInput = inputOTP_5;
                    // console.log(event.target.value);        
                    break;
                case (integer === 6 && event.target.value !== ''):
                    otpInput = inputOTP_6;
                    // console.log(event.target.value);        
                    break;
                default:
                    break;
            }
            otpInput.focus();
        }
        else{
            // console.log('else')
            var inputNameCountElse = inputName.substring(inputName.length-1, inputName.length);
            var integerElse = parseInt(inputNameCountElse, 10);
            if(integerElse-1 !== 0){
                // console.log(integerElse)
                integerElse = integerElse-1;
                // console.log(integerElse)
            }
            var otpInputElse;

            switch (true) {
                case (integerElse === 1):
                    otpInputElse = inputOTP_1;
                    // console.log(event.target.value);  
                    break;
                case (integerElse === 2):
                    otpInputElse = inputOTP_2;
                    // console.log(event.target.value);        
                    break;
                case (integerElse === 3):
                    otpInputElse = inputOTP_3;
                    // console.log(event.target.value);             
                    break;
                case (integerElse === 4):
                    otpInputElse = inputOTP_4;      
                    // console.log(event.target.value);        
                    break;
                case (integerElse === 5):
                    otpInputElse = inputOTP_5;
                    // console.log(event.target.value);        
                    break;
                case (integerElse === 6):
                    otpInputElse = inputOTP_6;
                    // console.log(event.target.value);        
                    break;
                default:
                    break;
            }
            otpInputElse.focus();
        } 
    }

    sendSMSOTP(inputPhone, otp){
        // console.log('inputPhone :' + inputPhone);
        // console.log('otp :' + otp);
        // console.log('fnaId :' + this.params);
        sendOTP({phone: inputPhone,fnaId: this.getID, otpId: otp})  
        .then(response => {
            // console.log('then', response)
            // console.log('OTP Success');

        })
        .catch(error => {
            console.log('catch');
            console.log(error);

        });

    }

    sendURL(){
        sendSMSURL({fnaId: this.getID})
        .then(response => {
            // console.log('then', response)
            // console.log('Send Url success');

            successDownload({fnaId: this.getID})
            .then(responses => {
                this.isDownload = true;
                // console.log('then', responses)
                // console.log('Update FNA success');
            })
            .catch(error => {
                console.log('catch Update FNA');
                console.log(error);
            });
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }

    @track loadOSC14 = false;
    @track loadOSC16 = false;
    @track responseBodyOSC14;
    @track responseBodyOSC16;
    isSendSMS = false;

    callOSC(fnaId,sendSms){
        // console.log('call OSC Id : ',fnaId)

       
        const promises = [
            //CALL SERVICE OSC14
            callServiceOSC14({fnaActivityId: fnaId})
            .then(result =>{
                // console.log('Call Service OSC14 Complete')
                // console.log('result OSC14 : ', result)
                if(result !== null){
                    if(!(result.statusCode)){
                        return result;
                    }
                }                
            })
            .catch(err => {
                console.log('Failed to call Service OSC14')
                console.log(err)
            }),
            

            //CALL SERVICE OSC16
            callServiceOSC16({fnaActivityId: fnaId})
            .then(result =>{
                // console.log('call Service OSC16 Complete')
                // console.log('result OSC16 : ', result)
                if(result !== null){
                    if(!(result.statusCode)){
                        return result;
                    }
                } 
                
            })
            .catch(err => {
                console.log('Failed to call Service OSC16')
                console.log(err)
            })

        ];

        Promise.all(promises)
        .then(res =>{
            // console.log('res : ' , res);
            // console.log('res[0] : ' , res[0]);
            // console.log('res[1] : ' , res[1]);
            var loadOSC14, loadOSC16 = false;

            loadOSC14 = res[0] !== undefined? true: false;
            loadOSC16 = res[1] !== undefined? true: false;

            // console.log('sendSms : ' + sendSms);
            if(sendSms){
                // console.log('send URL');
                this.sendURL();
            }

            if(loadOSC14 || loadOSC16){
                // console.log('StampOffset');
                stampOffSet({fnaActivityId: this.getID, responseBodyOSC14: res[0], responseBodyOSC16: res[1]})
                .then(response => {
                    // console.log(response)
                    this.mapGroup(this.getID);
                    // this.loadingDisplay = 'hideLoading';
                })
                .catch(err => {
                    console.log('Failed to call Service OSC14 or OSC16')
                    console.log(err)
                    this.loadingDisplay = 'hideLoading';
                    this.bShowModal = true;
                    this.bShowModal1 = false;
                    this.bShowModal3 = true;
                    this.bShowModal2 = false;
                })
                                
            }else{
                this.loadingDisplay = 'hideLoading';
                this.bShowModal = true;
                this.bShowModal1 = false;
                this.bShowModal3 = true;
                this.bShowModal2 = false;
            }

        })
        



    }

    resetOTP(){
        let inputOTP_1 = this.template.querySelector(`[data-id="otpInput_1"]`);
        let inputOTP_2 = this.template.querySelector(`[data-id="otpInput_2"]`);
        let inputOTP_3 = this.template.querySelector(`[data-id="otpInput_3"]`);
        let inputOTP_4 = this.template.querySelector(`[data-id="otpInput_4"]`);
        let inputOTP_5 = this.template.querySelector(`[data-id="otpInput_5"]`);
        let inputOTP_6 = this.template.querySelector(`[data-id="otpInput_6"]`);
        let btnSubmitOTP = this.template.querySelector(`[data-id="btnSubmitOTP"]`);

        fnGenerateOTP({fnaId: this.getID})
        .then(response => {
            // console.log('then',response);
            this.textRef = response.Otp_Ref__c;
            this.textErr = '';
            inputOTP_1.value = '';
            inputOTP_2.value = '';
            inputOTP_3.value = '';
            inputOTP_4.value = '';
            inputOTP_5.value = '';
            inputOTP_6.value = '';

            inputOTP_1.removeAttribute("disabled");
            inputOTP_2.removeAttribute("disabled");
            inputOTP_3.removeAttribute("disabled");
            inputOTP_4.removeAttribute("disabled");
            inputOTP_5.removeAttribute("disabled");
            inputOTP_6.removeAttribute("disabled");
            btnSubmitOTP.removeAttribute("disabled");
            this.sendSMSOTP(this.phoneNumber, response.Id);
            this.btnResetOTP = 'showBtn';
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }

    checkExsiting(){
        fnaCheckExisting({fnaId: this.getID})
        .then(responses => {
            // console.log('Update FNA success');
            this.callOSC(this.getID,true);
            
        })
        .catch(error => {
            console.log('catch Update FNA');
            console.log(error);
        });
    }

    @track imgExit = '';
    @track imgExitInvert = '';
    ImageOverExit(element){
        // console.log('ImageOver')
        let imgCancel = this.template.querySelector(`[data-id="imgCancel"]`);
        imgCancel.src = this.imgExitInvert;
    }
    ImageUnOverExit(element){
        // console.log('ImageUnOver')
        let imgCancel = this.template.querySelector(`[data-id="imgCancel"]`);
        imgCancel.src = this.imgExit;
    }

}